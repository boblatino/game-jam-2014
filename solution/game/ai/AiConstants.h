#pragma once

// Radius and forward offset of the wander circle
#define WANDER_OFFSET ( 100.0f )
#define WANDER_RADIUS ( 50.0f )

// The maximum rate at which the wander orientation can change
#define WANDER_RATE   ( 0.1f )