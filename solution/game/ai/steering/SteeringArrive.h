#pragma once

#include <cstdint>

#include "math/Point3.h"
#include "math/Vector3.h"

struct SteeringOutput;

struct ArriveData {
    const Point3* mSrcPos;
    const Vector3* mSrcLinearVel;

    const Point3* mTargetPos;

    // Radius inside we consider we arrived
    // to target
    float mTargetRadius;

    // Radius inside we begin to 
    // slow down linear velocity.
    float mSlowDownRadius;

    // Time over which to achieve target speed
    float mTimeToTarget;

    ArriveData(const Point3* srcPos = nullptr,
        const Vector3* srcLinearVel = nullptr,
        const Point3* targetPos = nullptr,
        const float targetRadius = 0.0f,
        const float slowDownRadius = 0.0f,
        const float timeToTarget = 0.0f);

    void set(const Point3& srcPos,
        const Vector3& srcLinearVel,
        const Point3& targetPos,
        const float targetRadius,
        const float slowDownRadius,
        const float timeToTarget);
};

// Note: numData should be greater than zero
void arrive(const ArriveData * const data, 
            SteeringOutput * const outputs,
            const uint32_t numData);