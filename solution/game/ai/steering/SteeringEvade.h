#pragma once

#include <cstdint>

struct Point3;
struct Vector3;

struct SteeringOutput;

struct EvadeData{
    const Point3* mSrcPos;
    const Vector3* mSrcLinearVel;
    const Point3* mTargetPos;
    const Vector3* mTargetLinearVel;

    // Time (0.5f = 50% sec) to predict target position 
    // translation
    float mMaxPredictionTime;

    EvadeData(const Point3* srcPos = nullptr,
        const Vector3* srcLinearVel = nullptr,
        const Point3* targetPos = nullptr,
        const Vector3* targetLinearVel = nullptr,   
        const float maxPredictionTime = 0.0f);

    void set(const Point3& srcPos,
        const Vector3& srcLinearVel,
        const Point3& targetPos,
        const Vector3& targetLinearVel,
        const float maxPredictionTime);
};

// Note: numData should be greater than zero
void evade(EvadeData * const data, 
           SteeringOutput * const outputs,
           const uint32_t numData);