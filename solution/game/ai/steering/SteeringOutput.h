#pragma once

#include "math/Vector3.h"

//////////////////////////////////////////////////////////////////////////
//
// Structure where steering algorithms store their outputs
//
//////////////////////////////////////////////////////////////////////////

struct SteeringOutput {
    // Linear velocity: Speed of the entity
    Vector3 mLinearVel;

    SteeringOutput(const Vector3& linearVel = Vector3());
};