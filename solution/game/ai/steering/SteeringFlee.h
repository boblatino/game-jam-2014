#pragma once

#include <cstdint>

#include "math/Point3.h"

struct SteeringOutput;

struct FleeData {
    const Point3* mSrcPos;
    const Point3* mTargetPos;
    float mSpeed;

    FleeData(const Point3* srcPos = nullptr,
        const Point3* targetPos = nullptr,
        const float speed = 0.0f);

    void set(const Point3& srcPos,
        const Point3& targetPos,
        const float speed);
};

// Note: numData should be greater than zero
void flee(const FleeData * const data, 
          SteeringOutput * const outputs,
          const uint32_t numData);