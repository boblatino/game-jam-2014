#pragma once

#include <cstdint>

#include "math/Vector3.h"

//////////////////////////////////////////////////////////////////////////
//
// Steering behavior that tries that source linear velocity
// matches target linear velocity
//
//////////////////////////////////////////////////////////////////////////


struct SteeringOutput;

struct VelocityMatchData {
    const Vector3* mSrcLinearVel;

    const Vector3* mTargetLinearVel;

    // Time over which to achieve target speed
    float mTimeToTarget;

    VelocityMatchData(const Vector3* srcLinearVel = nullptr,
        const Vector3* targetLinearVel = nullptr,
        const float timeToTarget = 0.0f);

    void set(const Vector3& srcLinearVel,
        const Vector3& targetLinearVel,
        const float timeToTarget);
};

// Note: numData should be greater than zero
void velocityMatch(const VelocityMatchData * const data, 
                   SteeringOutput * const outputs,
                   const uint32_t numData);

