#pragma once

#include <cstdint>

//////////////////////////////////////////////////////////////////////////
//
// Wander behavior modifies the entity's orientation to get different
// linear velocities, which allows the character to wander as 
// it moves forward.
//
//////////////////////////////////////////////////////////////////////////

struct Point3;
struct Vector3;


struct SteeringOutput;

struct WanderData {
    const Point3* mSrcPos;;
    const Vector3* mSrcLinearVel;
    const float* mSrcOrientation;
    float mWanderOrientation;

    WanderData(const Point3* srcPos = nullptr,
        const Vector3* srcLinearVel = nullptr,
        const float* srcOrientation = nullptr);

    void set(const Point3& srcPos,
        const Vector3& srcLinearVel,
        const float& srcOrientation);
};

void wander(WanderData * const data, 
            SteeringOutput * const outputs,
            const uint32_t numData);