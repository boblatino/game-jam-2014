#pragma once

#include <cstdint>

#include "math/Point3.h"

struct SteeringOutput;

struct SeekData {
    const Point3* mSrcPos;
    const Point3* mTargetPos;
    float mSpeed;

    SeekData(const Point3* srcPos = nullptr,
        const Point3* targetPos = nullptr,
        const float speed = 0.0f);

    void set(const Point3& srcPos,
        const Point3& targetPos,
        const float speed);
};

// Note: numData should be greater than zero
void seek(const SeekData * const data, 
          SteeringOutput * const outputs,
          const uint32_t numData);