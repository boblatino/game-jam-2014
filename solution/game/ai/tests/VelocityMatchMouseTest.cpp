#include "VelocityMatchMouseTest.h"

#include <iostream>

#include <SDL.h>
#include <SDL_image.h>

#include "ai/steering/SteeringOutput.h"
#include "ai/steering/SteeringArrive.h"
#include "ai/steering/SteeringVelocityMatch.h"
#include "graphic/Drawer.h"
#include "input/MouseState.h"
#include "main/Globals.h"
#include "main/MainConstants.h"
#include "math/MathConstants.h"
#include "math/Point2.h"
#include "math/Vector3.h"
#include "math/Utils.h"
#include "physic/Kinematic.h"
#include "utils/Error.h"
#include "utils/Timer.h"

#define NUM_BEES_ARRIVE         ( 1 )
#define NUM_BEES_PURSUE ( 19 )
#define NUM_BEES                ( NUM_BEES_ARRIVE + NUM_BEES_PURSUE )

namespace {
    SDL_Texture* gLeaderTexture;
    SDL_Texture* gSubordinateTexture;
    TextureData gTextures[NUM_BEES];
    DynamicData gDrawData[NUM_BEES];

    Kinematic gKinematics[NUM_BEES];
    SteeringOutput gSteerings[NUM_BEES];
    ArriveData gArriveData[NUM_BEES_ARRIVE];
    VelocityMatchData gVelocityMatchData[NUM_BEES_PURSUE];
    Point3 gMousePos;

    bool init() {
        Globals::init();        

        gLeaderTexture = IMG_LoadTexture(Globals::gRenderer, "test_media/textures/butterfly1.png");
        gSubordinateTexture = IMG_LoadTexture(Globals::gRenderer, "test_media/textures/bee_small.png");
        if (!gLeaderTexture) {
            printError("IMG_LoadTexture failed");
            return false;
        }

        if (!gSubordinateTexture) {
            printError("IMG_LoadTexture failed");
            return false;
        }

        // Init textures
        gTextures[0].mTexture = gLeaderTexture;
        for (size_t i = NUM_BEES_ARRIVE; i < NUM_BEES; ++i) {
            gTextures[i].mTexture = gSubordinateTexture;
        }

        // Init kinematics
        {
            const float xPos = WINDOW_WIDTH / 2.0f;
            const float zPos = WINDOW_HEIGHT / 2.0f;
            for (uint32_t i = 0; i < NUM_BEES; ++i) {
                gKinematics[i].mPosition.set(xPos, 0.0f, zPos);
                gKinematics[i].mOrientation = 0.0f;
                gKinematics[i].mLinearVel.set(0.0f, 0.0f, 0.0f);
            }
        }

        // Init steering data
        gArriveData[0].mSrcPos = &gKinematics[0].mPosition;
        gArriveData[0].mSrcLinearVel = &gKinematics[0].mLinearVel;
        gArriveData[0].mTargetPos = &gMousePos;
        gArriveData[0].mTargetRadius = 1.0f;
        gArriveData[0].mSlowDownRadius = 100.0f;
        gArriveData[0].mTimeToTarget = 0.25f ;

        gVelocityMatchData[0].mSrcLinearVel = &gKinematics[1].mLinearVel;
        gVelocityMatchData[0].mTargetLinearVel = &gKinematics[0].mLinearVel;
        gVelocityMatchData[0].mTimeToTarget = 0.25f;

        for (uint32_t i = NUM_BEES_ARRIVE; i < NUM_BEES_PURSUE; ++i) {
            gVelocityMatchData[i].mSrcLinearVel = &gKinematics[i + 1].mLinearVel;
            gVelocityMatchData[i].mTargetLinearVel = &gKinematics[i].mLinearVel;
            gVelocityMatchData[i].mTimeToTarget = 0.25f;
        }

        // Init draw data
        for (uint32_t i = 0; i < NUM_BEES; ++i) {
            gDrawData[i].mCenterPosX = &gKinematics[i].mPosition.x;
            gDrawData[i].mCenterPosY = &gKinematics[i].mPosition.z;
            gDrawData[i].mOrientation = &gKinematics[i].mOrientation;
        }

        return true;
    }

    void execute() {
        bool loop = true;
        Globals::gTimer.reset();
        while (loop) {
            // Begin of frame tick
            Globals::gTimer.tick();

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    loop = false;
                    break;
                } else if (event.type == SDL_MOUSEMOTION) {
                    Globals::gMouseState.handleMouseMotionEvent(event.motion);
                }
            }

            // Update global mouse position (used as target in steering)
            gMousePos.set(static_cast<float> (Globals::gMouseState.mX),
                          0.0f,
                          static_cast<float> (Globals::gMouseState.mY));

            // Update mouse buttons state
            Globals::gMouseState.updateButtonsState();

            //Clear screen
            int result = SDL_SetRenderDrawColor(Globals::gRenderer, 0, 0, 0, 0 );
            if (result != 0) {
                printError("SDL_SetRenderDrawColor failed");
                loop = true;
                break;
            }

            result = SDL_RenderClear(Globals::gRenderer);
            if (result != 0) {
                printError("SDL_RenderClear failed");
                loop = true;
                break;
            }

            // Execute arrive and velocity match steering behaviors
            arrive(gArriveData, gSteerings, NUM_BEES_ARRIVE);
            velocityMatch(gVelocityMatchData, 
                                              gSteerings + NUM_BEES_ARRIVE,
                                              NUM_BEES_PURSUE);

            // Update kinematics            
            integrate(gKinematics, gSteerings, NUM_BEES);

            // Draw 
            drawTextures(gDrawData, gTextures, NUM_BEES);

            //Update screen
            SDL_RenderPresent(Globals::gRenderer);

            // Clear mouse events states
            Globals::gMouseState.clearMouseEvents();

            // End of frame tick
            // Ensure current frame takes at least
            // MS_PER_FRAME
            Globals::gTimer.tick();           
            double elapsedTime = Globals::gTimer.deltaTime();
            while (elapsedTime < MS_PER_FRAME_DOUBLE) {
                Globals::gTimer.tick();
                elapsedTime += Globals::gTimer.deltaTime();
            }           
        }
    }

    void clear() {
        SDL_DestroyTexture(gLeaderTexture);
        SDL_DestroyTexture(gSubordinateTexture);
        Globals::destroy();
    }
}

namespace velocityMatchTest {
    void run() {
        const bool result = init();
        if (!result) {
            printError("velocityMatchTest init() failed", false);
            return;
        }

        execute();
        clear();
    }
}