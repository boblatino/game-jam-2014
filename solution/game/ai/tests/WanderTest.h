#pragma once

//////////////////////////////////////////////////////////////////////////
//
// The application show a group of bees.
// The leader moves according Wander behavior
// The subordinates moves according Arrive behavior
// Subordinate i follows subordinate i - 1
//
//////////////////////////////////////////////////////////////////////////

namespace wanderTest {
    void run();
}