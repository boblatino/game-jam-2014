#include "FleeingMouseTest.h"

#include <iostream>

#include <SDL.h>
#include <SDL_image.h>

#include "ai/steering/SteeringFlee.h"
#include "ai/steering/SteeringOutput.h"
#include "graphic/Drawer.h"
#include "input/MouseState.h"
#include "main/Globals.h"
#include "main/MainConstants.h"
#include "math/MathConstants.h"
#include "math/Point2.h"
#include "math/Vector3.h"
#include "math/Utils.h"
#include "physic/Kinematic.h"
#include "utils/Error.h"
#include "utils/Timer.h"

#define NUM_BEES ( 1 )

namespace {
    TextureData gTextures[NUM_BEES];
    DynamicData gDrawData[NUM_BEES];

    Kinematic gKinematics[NUM_BEES];
    SteeringOutput gSteerings[NUM_BEES];
    FleeData gFleeData[NUM_BEES];
    Point3 gMousePos;

    bool init() {
        Globals::init();
        Globals::gTimer.reset();

        SDL_Texture* texture;
        texture = IMG_LoadTexture(Globals::gRenderer, "test_media/textures/bee_small.png");
        if (!texture) {
            printError("IMG_LoadTexture failed");
            return false;
        } else {
            // Init textures
            for (size_t i = 0; i < NUM_BEES; ++i) {
                gTextures[i].mTexture = texture;
            }

            // Init kinematics
            gKinematics[0].mPosition.set(WINDOW_WIDTH / 2, 0.0f, WINDOW_HEIGHT / 2);
            gKinematics[0].mOrientation = 0.0f;
            gKinematics[0].mLinearVel.set(0.0f, 0.0f, 0.0f);

            // Init steering behaviors
            gFleeData[0].mSrcPos = &gKinematics[0].mPosition;
            gFleeData[0].mTargetPos = &gMousePos;
            gFleeData[0].mSpeed = 40.0f;

            // Init draw data
            gDrawData[0].mCenterPosX = &gKinematics[0].mPosition.x;
            gDrawData[0].mCenterPosY = &gKinematics[0].mPosition.z;
            gDrawData[0].mOrientation = &gKinematics[0].mOrientation;
        }

        return true;
    }

    void execute() {
        bool loop = true;
        Globals::gTimer.reset();
        while (loop) {
            // Begin of frame tick
            Globals::gTimer.tick();

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    loop = false;
                    break;
                } else if (event.type == SDL_MOUSEMOTION) {
                    Globals::gMouseState.handleMouseMotionEvent(event.motion);
                }
            }

            // Update global mouse position (used as target in steering)
            gMousePos.set(static_cast<float> (Globals::gMouseState.mX),
                          0.0f,
                          static_cast<float> (Globals::gMouseState.mY));

            // Update mouse buttons state
            Globals::gMouseState.updateButtonsState();

            //Clear screen
            int result = SDL_SetRenderDrawColor(Globals::gRenderer, 0, 0, 0, 0 );
            if (result != 0) {
                printError("SDL_SetRenderDrawColor failed");
                loop = true;
                break;
            }

            result = SDL_RenderClear(Globals::gRenderer);
            if (result != 0) {
                printError("SDL_RenderClear failed");
                loop = true;
                break;
            }

            // Execute flee steering behavior
            flee(gFleeData, gSteerings, NUM_BEES);

            // Update kinematics
            const float dt = Globals::gTimer.deltaTime();
            integrate(gKinematics, 
                              gSteerings, 
                              NUM_BEES);

            // Draw 
            drawTextures(gDrawData, gTextures, NUM_BEES);

            // Print mouse position
            std::cout << "Mouse Position = ( " << Globals::gMouseState.mX;
            std::cout << " , " << Globals::gMouseState.mY << " )" << std::endl;

            //Update screen
            SDL_RenderPresent(Globals::gRenderer);

            // Clear mouse events states
            Globals::gMouseState.clearMouseEvents();

            // End of frame tick
            // Ensure current frame takes at least
            // MS_PER_FRAME
            Globals::gTimer.tick();           
            double elapsedTime = Globals::gTimer.deltaTime();
            while (elapsedTime < MS_PER_FRAME_DOUBLE) {
                Globals::gTimer.tick();
                elapsedTime += Globals::gTimer.deltaTime();
            }  
        }
    }

    void clear() {
        Globals::destroy();
    }
}

namespace fleeingMouseTest {
    void run() {
        const bool result = init();
        if (!result) {
            printError("fleeingMouseTest init() failed", false);
            return;
        }
        execute();
        clear();
    }
}