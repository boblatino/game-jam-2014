#pragma once

#include <cstdint>

//////////////////////////////////////////////////////////////////////////
//
// Mouse State class that stores:
//    - mouse position 
//    - mouse button events (recent button pressed or released)
//
//////////////////////////////////////////////////////////////////////////

struct SDL_MouseButtonEvent;
struct SDL_MouseMotionEvent;

enum class mouseButtonEventState : uint8_t {
    NO_EVENT = 0,
    RELEASED,
    PRESSED
};

enum class mouseButtonState : uint8_t {
    NOT_PRESSED = 0,
    PRESSED
};

struct MouseState {
    int mX;
    int mY;

    // Left, Middle and Right buttons state
    mouseButtonState mMouseButtonState[3];

    // Left, Middle and Right buttons event state
    mouseButtonEventState mMouseButtonEventState[3];

    MouseState();

    // Init mouse state using SDL
    // Be sure SDL is correctly initialized
    // This should be called once
    void init();

    // Update mouse buttons state through SDL
    // This should be called at the beginning of each frame
    void updateButtonsState();

    // Clears mouse events state
    // This should be called at the end of each frame
    // to avoid handling already processed events
    void clearMouseEvents();

    // Updates mouse buttons state according a mouse button event
    void handleMouseButtonEvent(const SDL_MouseButtonEvent& event);

    // Updates mouse position state according a mouse motion event
    void handleMouseMotionEvent(const SDL_MouseMotionEvent& event);

private:
    MouseState(const MouseState& src);
    const MouseState& operator=(const MouseState& src);
};

