#include "InputUnitTests.h"

#include <cassert>

#include "input/KeyboardState.h"
#include "input/MouseState.h"

namespace {
    void keyboardStateUnitTests() {
        // Constructor():
        {
            const KeyboardState keyboardState;
            assert(keyboardState.mKeysState == nullptr);
            assert(keyboardState.mKeyEventState == nullptr);
        }
    }

    void mouseStateUnitTests() {
        // Constructor():
        {
            const MouseState mouseState;
            assert(mouseState.mX == 0);
            assert(mouseState.mY == 0);

            for (uint8_t i = 0; i < 3; ++i) {
                assert(mouseState.mMouseButtonState[i] == mouseButtonState::NOT_PRESSED);
                assert(mouseState.mMouseButtonEventState[i] == 
                       mouseButtonEventState::NO_EVENT);
            }

        }

        // clearMouseEvents():
        {
            MouseState mouseState;
            mouseState.clearMouseEvents();
            for (uint8_t i = 0; i < 3; ++i) {
                assert(mouseState.mMouseButtonEventState[i] == 
                       mouseButtonEventState::NO_EVENT);
            }
        }
    }
}

namespace inputUnitTests {
    void runTests() {
        keyboardStateUnitTests();
        mouseStateUnitTests();
    }
}