#include "MouseMotionTest.h"

#include <iostream>

#include <SDL.h>

#include "input/MouseState.h"
#include "main/Globals.h"
#include "utils/Error.h"
#include "utils/Timer.h"

namespace {
    bool init() {
        Globals::init();
        Globals::gTimer.reset();

        return true;
    }

    void execute() {
        bool loop = true;
        while (loop) {
            Globals::gTimer.tick();

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    loop = false;
                    break;
                } else if (event.type == SDL_MOUSEMOTION) {
                    Globals::gMouseState.handleMouseMotionEvent(event.motion);
                }
            }

            // Update mouse buttons state
            Globals::gMouseState.updateButtonsState();

            //Clear screen
            int result = SDL_SetRenderDrawColor(Globals::gRenderer, 0, 0, 0, 0 );
            if (result != 0) {
                printError("SDL_SetRenderDrawColor failed");
                loop = true;
                break;
            }

            result = SDL_RenderClear(Globals::gRenderer);
            if (result != 0) {
                printError("SDL_RenderClear failed");
                loop = true;
                break;
            }

            // Print mouse position
            std::cout << "Mouse Position = ( " << Globals::gMouseState.mX;
            std::cout << " , " << Globals::gMouseState.mY << " )" << std::endl;

            //Update screen
            SDL_RenderPresent(Globals::gRenderer);

            // Clear mouse events states
            Globals::gMouseState.clearMouseEvents();
        }
    }

    void clear() {
        Globals::destroy();
    }
}

namespace mouseMotionTest {
    void run() {
        const bool result = init();
        if (!result) {
            printError("mouseMotionTest init() failed", false);
            return;
        }
        execute();
        clear();
    }
}