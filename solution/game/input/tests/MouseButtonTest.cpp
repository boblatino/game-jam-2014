#include "MouseMotionTest.h"

#include <iostream>

#include <SDL.h>

#include "input/MouseState.h"
#include "main/Globals.h"
#include "utils/Error.h"
#include "utils/Timer.h"

namespace {
    bool init() {
        Globals::init();
        Globals::gTimer.reset();

        return true;
    }

    void execute() {
        bool loop = true;
        while (loop) {
            Globals::gTimer.tick();

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    loop = false;
                    break;
                } else if (event.type == SDL_MOUSEBUTTONDOWN ||
                           event.type == SDL_MOUSEBUTTONUP) {
                    Globals::gMouseState.handleMouseButtonEvent(event.button);
                }
            }

            // Update mouse buttons state
            Globals::gMouseState.updateButtonsState();

            //Clear screen
            int result = SDL_SetRenderDrawColor(Globals::gRenderer, 0, 0, 0, 0 );
            if (result != 0) {
                printError("SDL_SetRenderDrawColor failed");
                loop = true;
                break;
            }

            result = SDL_RenderClear(Globals::gRenderer);
            if (result != 0) {
                printError("SDL_RenderClear failed");
                loop = true;
                break;
            }

            // Print mouse event if any
            if (Globals::gMouseState.mMouseButtonEventState[0] != 
                mouseButtonEventState::NO_EVENT) 
            {
                if (Globals::gMouseState.mMouseButtonEventState[0] == 
                    mouseButtonEventState::PRESSED) 
                {
                    std::cout << "LEFT Mouse Button PRESSED" << std::endl;
                } else {
                    std::cout << "LEFT Mouse Button RELEASED" << std::endl;
                }
            }

            if (Globals::gMouseState.mMouseButtonEventState[1] != 
                mouseButtonEventState::NO_EVENT) 
            {
                if (Globals::gMouseState.mMouseButtonEventState[1] == 
                    mouseButtonEventState::PRESSED) 
                {
                    std::cout << "MIDDLE Mouse Button PRESSED" << std::endl;
                } else {
                    std::cout << "MIDDLE Mouse Button RELEASED" << std::endl;
                }
            }

            if (Globals::gMouseState.mMouseButtonEventState[2] != 
                mouseButtonEventState::NO_EVENT) 
            {
                if (Globals::gMouseState.mMouseButtonEventState[2] == 
                    mouseButtonEventState::PRESSED) 
                {
                    std::cout << "RIGHT Mouse Button PRESSED" << std::endl;
                } else {
                    std::cout << "RIGHT Mouse Button RELEASED" << std::endl;
                }
            }

            //Update screen
            SDL_RenderPresent(Globals::gRenderer);

            // Clear mouse events states
            Globals::gMouseState.clearMouseEvents();

        }
    }

    void clear() {
        Globals::destroy();
    }
}

namespace mouseButtonsTest {
    void run() {
        const bool result = init();
        if (!result) {
            printError("mouseButtonsTest init() failed", false);
            return;
        }
        execute();
        clear();
    }
}