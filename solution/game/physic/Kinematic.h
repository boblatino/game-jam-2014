#pragma once

#include "math/Point3.h"
#include "math/Vector3.h"

//////////////////////////////////////////////////////////////////////////
//
// Represents the position, orientation and movement of an entity
//
//////////////////////////////////////////////////////////////////////////

struct SteeringOutput;

struct Kinematic {
    // (x, y, z) where x and z are matched to (x, y) respectively 
    // in SDL
    Point3 mPosition;

    // Angle in radians around y axis in right-handed coordinate system 
    // (counterclockwise from z to x axis)
    float mOrientation;

    // Linear velocity: Speed of the entity
    Vector3 mLinearVel;

    Kinematic(const Point3& position = Point3(),
        const float orientation = 0.0f,
        const Vector3& linearVel = Vector3());
};

// Perfoms a forward Euler integration of the Kinematic 
// applying the given acceleration
// from steering output.
// Because the integration is Euler, all the acceleration is
// applied to the velocity at the end of the time step.
void integrate(Kinematic * const kinematics,
               const SteeringOutput * const steeringOutputs,
               const size_t numKinematics,
               const float* limitSpeed = nullptr);