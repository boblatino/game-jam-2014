#include "PhysicUnitTests.h"

#include <cassert>

#include "ai/steering/SteeringOutput.h"
#include "math/Point3.h"
#include "math/Utils.h"
#include "math/Vector3.h"
#include "physic/Kinematic.h"

namespace physicUnitTests {
    void runTests() {
        kinematicTests();
    }

    void kinematicTests() {
        // Constructor
        {
            const Point3 point(0.0f, 1.0f, 2.0f);
            const float orientation = -15.0f;
            const Vector3 linearVel(1.0f, 1.0f, 1.0f);
            Kinematic kinematic(point, orientation, linearVel);

            assert(point == kinematic.mPosition);

            bool result = areEquals(orientation, kinematic.mOrientation);
            assert(result == true); 

            assert(linearVel == kinematic.mLinearVel); 
        }
    }
}