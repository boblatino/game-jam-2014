#pragma once

#include "main/MainConstants.h"
#include "math/MathConstants.h"

#define MAX_SPEED         ( 200.0f )

#define ANGULAR_VEL_RAD_PER_SEC   ( PI / 2.0f )
#define ANGULAR_VEL_RAD_PER_FRAME ( ANGULAR_VEL_RAD_PER_SEC * MS_PER_FRAME_FLOAT )