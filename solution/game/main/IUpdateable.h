#pragma once

struct Iupdateable {
	virtual void update() = 0;
};