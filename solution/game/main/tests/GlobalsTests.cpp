#include "GlobalsTests.h"


#include <SDL.h>
#include <iostream>

#include "main/Globals.h"
#include "utils/Error.h"
#include "utils/Timer.h"

namespace globalsTests{
    void run() {
        const bool result = Globals::init();
        if (!result) {
            printError("Globals::init() failed", false);
            return;
        }

        // Main loop
        Globals::gTimer.reset();
        bool loop = true;
        while (loop) {
            Globals::gTimer.tick();

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    loop = false;
                    break;
                }
            }

            //Clear screen
            SDL_SetRenderDrawColor(Globals::gRenderer, 0xFF, 0xFF, 0xFF, 0xFF );
            SDL_RenderClear(Globals::gRenderer);

            //Render red filled quad
            SDL_Rect fillRect = { 1024 / 4, 768 / 4, 1024 / 2, 768 / 2 };
            SDL_SetRenderDrawColor( Globals::gRenderer, 0xFF, 0x00, 0x00, 0xFF );		
            SDL_RenderFillRect( Globals::gRenderer, &fillRect );

            //Update screen
            SDL_RenderPresent( Globals::gRenderer );

            std::cout << "Elapsed time = " << Globals::gTimer.deltaTime() << std::endl;
        }

        Globals::destroy();
    }
}