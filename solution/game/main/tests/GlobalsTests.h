#pragma once

//////////////////////////////////////////////////////////////////////////
//
// Testing globals initialization and destruction
//    - Show the elapsed frame time in the console
//    - Show a red rectangle at the screen
//    - You can see the main loop functionality in source code
//
//////////////////////////////////////////////////////////////////////////

namespace globalsTests{
    void run();
}