#pragma once

#include <string>

#include "input/KeyboardState.h"
#include "input/MouseState.h"
#include "utils/Timer.h"
#include <cassert>
#include <collisions/CollisionManager.h>
#include <main/GameLoop.h>
#include <utils/ConstantManager.h>


//////////////////////////////////////////////////////////////////////////
//
// Global variables used by most parts of the application
//
//////////////////////////////////////////////////////////////////////////

struct SDL_Renderer;
struct SDL_Window;
class CharacterManager;
class DrawManager;
class GameStates;
class ResourceManager;
class StateManager;

struct Globals {
    static SDL_Window* gWindow;
    static SDL_Renderer* gRenderer;
    static Timer gTimer;
    static KeyboardState gKeyboardState;
    static MouseState gMouseState;
	static CollisionManager* gCollisionManager;
	static GameLoop* gGameLoop;
	static ConstantManager* gConstantManager;
	static DrawManager* gDrawManager;
	static ResourceManager* gResourceManager;
	static std::string gExecutablePath;
    static GameStates* gGameStates;
    static StateManager* gStateManager;   
    static CharacterManager* gCharacterManager;

    // Initializes global variables.
    // Returns true if initialization was successful
    static bool init();

    // Destroy or deinitializes global variables
    static void destroy();

private:
    Globals();
    Globals(const Globals& src);
    Globals& operator=(const Globals& src);
};

