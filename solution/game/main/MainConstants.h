#pragma once

#define WINDOW_WIDTH  ( 1024 )
#define WINDOW_HEIGHT ( 768 )

#define MAX_FPS (60)
#define MS_PER_FRAME_FLOAT (1.0f / MAX_FPS)
#define MS_PER_FRAME_DOUBLE (1.0 / MAX_FPS)

#define BOX_SIZE 618
#define PIXEL_TO_METER 0.02f
#define METER_TO_PIXEL 50.f
