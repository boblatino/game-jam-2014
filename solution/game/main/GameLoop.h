#pragma once
#include "IUpdateable.h"
#include <vector>

class GameLoop {
public:
	GameLoop();
	void loop();
	void stop();

	// Call here to add your class to the main loop
	void updateMe(Iupdateable* updateable);
	void removeMe(Iupdateable* updateable);

private: 
	bool run;
	std::vector<Iupdateable*> updateables;
	std::vector<Iupdateable*> queuedInserts;
	std::vector<Iupdateable*> queuedDeletes;
	bool isUpdating;


};