#include "Entity.h"

#include <SDL_mixer.h>

#include <graphic/AnimatedDrawable.h>
#include <graphic/Drawable.h>
#include <main/MainConstants.h>
#include <main/Globals.h>
#include <states/Level1State.h>
#include <states/GameStates.h>
#include <utils/ResourceManager.h>
#include <states/GameStates.h>

void Entity::collide(Entity* other) {

    if ((type == PLAYER && other->type == ENEMY) ||
        (type == ENEMY && other->type == PLAYER) ||
        (type == PLAYER && other->type == MINES) ||
        (type == MINES && other->type == PLAYER)) { 
            if (!Globals::gCharacterManager->removeChild()) {
                Level1State* level = static_cast<Level1State*> (Globals::gGameStates->mLevel1State);
                level->mEndLevel = true;                
                level->mFramesToWaitBeforeLeave = 3.0 * MAX_FPS;
            }      
    }   

    if ((type == PLAYER && other->type == CHILD)) {
		other->type = NONE;
        Mix_PlayChannel(-1, Globals::gResourceManager->getSound("addChild"), 0);
        Globals::gCharacterManager->addChild(static_cast<AnimatedDrawable*> (other->mDrawable));
   
        Level1State* level = static_cast<Level1State*> (Globals::gGameStates->mLevel1State);
        level->mChildrenCatched++;
    } else if ((type == CHILD && other->type == PLAYER)) {
		type = NONE;
        Mix_PlayChannel(-1, Globals::gResourceManager->getSound("addChild"), 0);
        Globals::gCharacterManager->addChild(static_cast<AnimatedDrawable*> (mDrawable));

        Level1State* level = static_cast<Level1State*> (Globals::gGameStates->mLevel1State);
        level->mChildrenCatched++;
    } 

	 if ((type == PLAYER && other->type == DOOR)) {
		Globals::gGameStates->changeLevel();
    } else if ((type == DOOR && other->type == PLAYER)) {
		Globals::gGameStates->changeLevel();
    } 
}

Entity::Entity() 
    : type(NONE)
    , id(0)
    , staticObject(false)
    , pisycs(false)
    , mDrawable(nullptr)
{

}