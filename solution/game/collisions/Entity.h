#pragma once

#include <main/Globals.h>
#include <utils/CharacterManager.h>

class Drawable;

enum ENTITY_TYPE {
	NONE,
	DOOR,
    ENEMY,
    PLAYER,
    MINES,
    CHILD,
};


struct Entity {
	ENTITY_TYPE type;
	size_t id;
	bool staticObject;
	bool pisycs;
    Drawable* mDrawable;
	void collide(Entity* other);

    Entity();
};