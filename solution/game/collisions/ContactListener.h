#pragma once
#include <Dynamics/b2WorldCallbacks.h>
#include <iostream>

class Entity;

class ContactListener : public b2ContactListener
{
	void getEntity(b2Contact* contact,  Entity*& entityA, Entity*& entityB);
	void BeginContact(b2Contact* contact);
	void EndContact(b2Contact* contact);
};