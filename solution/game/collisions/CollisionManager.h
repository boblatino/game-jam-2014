#pragma once

#include <main/IUpdateable.h>
#include <collisions\ContactListener.h>
#include <vector>

class Box2dDrawDebug;
class b2World;
class b2Body;

class CollisionManager : public Iupdateable {
public:
	CollisionManager();	
	~CollisionManager();	

	b2Body* createCollider(float x, float y, bool phisicsOn = false, bool staticBody = false);

	void update(); 

	void drawDebug();

	void createGroundBody();

	void destroy(b2Body*);

private:
	b2World* world;
	ContactListener contactListener;
	Box2dDrawDebug* debugDraw;
	b2Body* top; 
	b2Body* bottom;
	b2Body* left; 
	b2Body* right;

	std::vector<b2Body*> queueForDelete;

};