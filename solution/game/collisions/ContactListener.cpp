#include "ContactListener.h"
#include <collisions/Entity.h>
#include <Dynamics/Contacts/b2Contact.h>

void ContactListener::getEntity(b2Contact* contact,  Entity*& entityA, Entity*& entityB)  {
	b2Fixture* fixtureA = contact->GetFixtureA();
    b2Fixture* fixtureB = contact->GetFixtureB();
  
    entityA = static_cast<Entity*>(fixtureA->GetBody()->GetUserData());
    entityB = static_cast<Entity*>(fixtureB->GetBody()->GetUserData());
}

void ContactListener::BeginContact(b2Contact* contact) {
	/* Ball* radarEntity;
	Ball* aircraftEntity;
	if ( getRadarAndAircraft(contact, radarEntity, aircraftEntity) )
		radarEntity->radarAcquiredEnemy( aircraftEntity );*/

	Entity* entity1;
	Entity* entity2;
	getEntity(contact, entity1, entity2);

    if (!entity1 || !entity2) {
        return;
    }

	entity1->collide(entity2);

	//std::cout << "Begin contact" << std::endl;
}
  
void ContactListener:: EndContact(b2Contact* contact) {
	/*  Ball* radarEntity;
	Ball* aircraftEntity;
	if ( getRadarAndAircraft(contact, radarEntity, aircraftEntity) )
		radarEntity->radarLostEnemy( aircraftEntity );*/
	//std::cout << "End contact" << std::endl;
}