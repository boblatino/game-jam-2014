#include "GameStates.h"

#include <states/CreditsState.h>
#include "states/Level1State.h"
#include <states/PresentationState.h>
#include <states/TutorialState.h>

#include <states/NewGameState.h>

GameStates::GameStates() 
    : mLevel1State(new Level1State())
    , mPresentationState(new PresentationState())
    , mTutorialState(new TutorialState())
    , mCreditsState(new CreditsState())
    , mNewGameState(new NewGameState())
{

}



void GameStates::changeLevel() {
	Level1State* level = (Level1State*)mLevel1State;
	level->changeLevel();
}

GameStates::~GameStates() {
    delete mLevel1State;
    delete mPresentationState;
    delete mTutorialState;
    delete mCreditsState;
    delete mNewGameState;
}