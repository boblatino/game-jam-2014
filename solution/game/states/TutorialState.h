#pragma once

#include <states/GameState.h>

class VLCPlayer;

class TutorialState : public GameState {
public:
    void init();
    void update();
    void clear();

    VLCPlayer* mPlayer;
};