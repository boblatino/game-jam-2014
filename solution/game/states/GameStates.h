#pragma once

//////////////////////////////////////////////////////////////////////////
//
// All states belonging to the game
//
//////////////////////////////////////////////////////////////////////////

class GameState;

class GameStates {
public:
    GameStates();
    ~GameStates();

	void changeLevel();

    GameState* mLevel1State;
    GameState* mPresentationState;
    GameState* mTutorialState;
    GameState* mCreditsState;
    GameState* mNewGameState;
};