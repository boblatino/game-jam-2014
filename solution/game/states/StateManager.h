#pragma once

//////////////////////////////////////////////////////////////////////////
//
// Manages game states like presentation, credits, level X, etc
// - We can initialize an state and each state knows which is
//   its next state
//
//////////////////////////////////////////////////////////////////////////

#include <main/IUpdateable.h>

class GameState;

class StateManager : Iupdateable {
public:    
    StateManager();
    ~StateManager();

    void init();
    void update();

    void changeState(GameState &state);

private:
    GameState *mCurState; 
    GameState *mNewState;
};
