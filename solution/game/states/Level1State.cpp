#include "Level1State.h"

#include <SDL_mixer.h>

#include <cassert>

#include <graphic/DrawManager.h>
#include <input/KeyboardState.h>
#include <main/Globals.h>
#include <main/MainConstants.h>
#include <states\GameStates.h>
#include <states/CreditsState.h>
#include <states/PresentationState.h>
#include <states/StateManager.h>
#include <utils\CharacterManager.h>
#include <utils\EnemyGenerator.h>
#include <utils/ResourceManager.h>

#include <iostream>

Level1State::Level1State() 
	: mLevelGenerator(nullptr), enemyGenerator(nullptr), level(1)
{
    mRewindMusic = true;

	LevelData level1 = {0, 2, 0, 1, 1};
	LevelData level2 = {0, 2, 0, 3, 1};
	LevelData level3 = {1, 1, 0, 4, 1};
	LevelData level4 = {1, 1, 0, 8, 1};
	LevelData level5 = {2, 1, 0, 4, 1};
	LevelData level6 = {2, 2, 0, 10, 1};
    LevelData level7 = {2, 2, 0, 5, 1};
    LevelData level8 = {2, 2, 0, 10, 1};
    LevelData level9 = {3, 1, 0, 5, 1};
    LevelData level10 = {3, 1, 0, 10, 1};
    LevelData level11 = {3, 2, 0, 5, 1};
    LevelData level12 = {4, 2, 0, 10, 1};

	levelData[1] = level1;
	levelData[2] = level2;
	levelData[3] = level3;
	levelData[4] = level4;
	levelData[5] = level5;
	levelData[6] = level6;
	levelData[7] = level7;
    levelData[8] = level8;
    levelData[9] = level9;
    levelData[10] = level10;
    levelData[11] = level11;
    levelData[12] = level12;
}

void Level1State::changeLevel() {
	if(mLevelGenerator && mGoThroughDoor) {
		mLevelGenerator->changeLevel();
	}
}

void Level1State::init() {
    mEndLevel = false;   
    mChildrenCatched = 0;
    mGoThroughDoor = false;

    if (mRewindMusic) {
        Mix_PlayMusic(Globals::gResourceManager->getMusic("music"), -1);
    }

    // Create background drawable
    mBackground = new Drawable("background");
    mBackground->updatePosition(WINDOW_WIDTH / 2, WINDOW_HEIGHT / 2);
    Globals::gDrawManager->addDrawable(mBackground);

    assert(mLevelGenerator == nullptr);
    mLevelGenerator = new LevelGenerator(this);
	enemyGenerator = new EnemyGenerator(levelData[level].enemyCount, levelData[level].enemySpeed);
	enemyGenerator->generateStaticObjects(levelData[level].staticObjsCount);
	enemyGenerator->generateChilds(levelData[level].childCount, levelData[level].enemySpeed);
    Globals::gCharacterManager->init();
}

void Level1State::update() {
    if (mEndLevel) {
        if (mFramesToWaitBeforeLeave == 0) {
            Globals::gStateManager->changeState(*Globals::gGameStates->mCreditsState);
        } else {
            --mFramesToWaitBeforeLeave;
        }
    }

    if (mChildrenCatched == levelData[level].childCount) {
        mGoThroughDoor = true;
    }

    if (Globals::gKeyboardState.keyPressedEvent(SDL_SCANCODE_ESCAPE)) {
        Globals::gStateManager->changeState(*Globals::gGameStates->mCreditsState);
    }

    assert(mLevelGenerator);
    mLevelGenerator->update();
}

void Level1State::cleanEnemies() {
	delete enemyGenerator;
	enemyGenerator = nullptr;
}

void Level1State::clear() {
    //assert(mLevelGenerator);
    delete mLevelGenerator;
    mLevelGenerator = nullptr;
    Globals::gCharacterManager->clear();
	delete enemyGenerator;
    enemyGenerator = nullptr;

    Globals::gDrawManager->removeDrawable(mBackground);
    mBackground = nullptr;

    Globals::gDrawManager->clear();
    level = 1;

    if (mRewindMusic) {
        Mix_HaltMusic();
    }
}

void Level1State::rotationStart() {
	if(enemyGenerator != nullptr) {
		enemyGenerator->stopMotion();
	}	
}

void Level1State::finished() {
   const int lastLevel = level;
   mRewindMusic = false;
   clear();
   level = lastLevel + 1;
   init();
   mRewindMusic = true;
}

void Level1State::rotationEnd() {
	enemyGenerator->startMotion();
}