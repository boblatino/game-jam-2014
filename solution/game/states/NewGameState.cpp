#include "NewGameState.h"

#include <graphic/VLCPlayer.h>
#include <input/KeyboardState.h>
#include <main/Globals.h>
#include <main/MainConstants.h>
#include <states/GameStates.h>
#include <states/StateManager.h>
#include <states/TutorialState.h>

void NewGameState::init() {  
    mPlayer = new VLCPlayer("media/videos/newgame.mp4");
    mPlayer->play();
}

void NewGameState::update() {
    assert(mPlayer);
    if (mPlayer->isVideoStopped) {
        mPlayer->pause(true);
        Globals::gStateManager->changeState(*Globals::gGameStates->mTutorialState);
    }
}

void NewGameState::clear() {
    delete mPlayer;
}