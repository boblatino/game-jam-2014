#include "PresentationState.h"

#include <graphic/VLCPlayer.h>
#include <input/KeyboardState.h>
#include <main/Globals.h>
#include <main/MainConstants.h>
#include <states/GameStates.h>
#include <states/StateManager.h>
#include <states/NewGameState.h>

void PresentationState::init() {
    mPlayer = new VLCPlayer("media/videos/presentation.mov");
    mPlayer->play();
}

void PresentationState::update() {
    assert(mPlayer);
    if (mPlayer->isVideoStopped) {
        mPlayer->pause(true);
        Globals::gStateManager->changeState(*Globals::gGameStates->mNewGameState);
    }
}

void PresentationState::clear() {
    delete mPlayer;
}