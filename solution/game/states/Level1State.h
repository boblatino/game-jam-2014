#pragma once

//////////////////////////////////////////////////////////////////////////
//
// Level 1 State
//
//////////////////////////////////////////////////////////////////////////

#include "states/GameState.h"
#include <utils/LevelGenerator.h>
#include <map>

#include <cstdint>

struct LevelData {
	size_t enemyCount;
	size_t enemySpeed;
	size_t staticObjsCount;
	size_t childCount;
	size_t childSpeed;
};

class LevelGenerator;
class EnemyGenerator;
class Drawable;

class Level1State : public GameState, private ILevelGeneratorEvents {
public:
    Level1State();

    void init();
    void update();
    void clear();
	void initLevel();

	virtual void rotationStart();
	virtual void rotationEnd();
	virtual void cleanEnemies();
	virtual void finished();

	void changeLevel();

    LevelGenerator* mLevelGenerator;
	EnemyGenerator* enemyGenerator;

	std::map<size_t, LevelData> levelData;

	size_t level;

    bool mRewindMusic;

    Drawable* mBackground;

    bool mEndLevel;
    uint32_t mFramesToWaitBeforeLeave;

    bool mGoThroughDoor;
    uint32_t mChildrenCatched;
};