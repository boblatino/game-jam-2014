#include "CreditsState.h"

#include <graphic/VLCPlayer.h>
#include <input/KeyboardState.h>
#include <main/Globals.h>
#include <main/MainConstants.h>
#include <states/GameStates.h>
#include <states/StateManager.h>
#include <states/PresentationState.h>

void CreditsState::init() {  
    mPlayer = new VLCPlayer("media/videos/credits.mov");
    mPlayer->play();
}

void CreditsState::update() {
    assert(mPlayer);
    if (mPlayer->isVideoStopped) {
        mPlayer->pause(true);
        Globals::gStateManager->changeState(*Globals::gGameStates->mPresentationState);
    }
}

void CreditsState::clear() {
    delete mPlayer;
}