#pragma once

//////////////////////////////////////////////////////////////////////////
//
// Game state like presentation, levelX, credits, game over, etc
// - Used by StateManager
// - They should know where is its next state
// - They should request StateManager to change to the next state
// - Game state interface, you should implement its methods
//
//////////////////////////////////////////////////////////////////////////

class GameState {
public:
    virtual void init() = 0;
    virtual void update() = 0;
    virtual void clear() = 0;
};