#include "TutorialState.h"

#include <graphic/VLCPlayer.h>
#include <input/KeyboardState.h>
#include <main/Globals.h>
#include <main/MainConstants.h>
#include <states/GameStates.h>
#include <states/StateManager.h>
#include <states/Level1State.h>

void TutorialState::init() {
    mPlayer = new VLCPlayer("media/videos/tutorial.mov");
    mPlayer->play();
}

void TutorialState::update() {
    assert(mPlayer);
    if (mPlayer->isVideoStopped) {
        mPlayer->pause(true);
        Globals::gStateManager->changeState(*Globals::gGameStates->mLevel1State);
    }
}

void TutorialState::clear() {
    delete mPlayer;
}