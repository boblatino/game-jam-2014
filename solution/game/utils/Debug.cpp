#include "Debug.h"

#include <iostream>

#include "math/Point2.h"
#include "math/Point3.h"
#include "math/Vector3.h"

void printPoint2(const Point2& p, const char* info) {
    std::cout << info << ": " << std::endl;
    std::cout << "( " << p.x << " , " << p.y << " )" << std::endl; 
}

void printPoint3(const Point3& p, const char* info) {
    std::cout << info << std::endl;
    std::cout << "( " << p.x << " , " << p.y << " , " << p.z << " )" << std::endl; 
}

void printVector3(const Vector3& v, const char* info) {
    std::cout << info << ": " << std::endl;
    std::cout << "( " << v.x << " , " << v.y << " , " << v.z << " )" << std::endl;
}