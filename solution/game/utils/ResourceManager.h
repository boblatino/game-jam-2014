#pragma once
#include <map>
#include <SDL_mixer.h>

struct SDL_Texture;

class ResourceManager {
public:
	ResourceManager();
	~ResourceManager();
	SDL_Texture* getTexture(const std::string& id);
    Mix_Chunk* getSound(const std::string& id);
    Mix_Music* getMusic(const std::string& id);
	
private:
	void loadTexture(const std::string& fileName, const std::string& id, bool isTest = false);
	std::map<std::string, SDL_Texture*> textures;

    void loadSound(const std::string& filename,
                   const std::string& id);
    std::map<std::string, Mix_Chunk*> sounds;

    void loadMusic(const std::string& filename,
                   const std::string& id);
    std::map<std::string, Mix_Music*> music;

};