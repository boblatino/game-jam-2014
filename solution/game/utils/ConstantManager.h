#pragma once
#include <Windows.h>
#include <string>
#include <vector>

/*
THIS IS A CONFIG FILE EXAMPLE
int,0
float,2.25
string,Hola como andas
float,5.25, ESTO ES UN COMENTARIO
*/

// CREATE THIS FOLDER AND ADD A config.txt file there
#define PATH_TO_MONITOR "C:\\config.txt"
#define IS_GAME_IN_DEVELOPMENT 1

// WARNING!! THE ORDER MATTERS. THIS SHOULD HAVE THE SAME ORDER IN THE FILE
enum INT_VARS {
	FRAME_RATE,
};

enum FLOAT_VARS {
	VELOCITY,
};

enum STRING_VARS {
	APP_NAME
};

class ConstantManager {
public:

	// Accessors
	int getInt(INT_VARS val); 
	float getFloat(FLOAT_VARS val);
	std::string getString(STRING_VARS val);

	ConstantManager();
	~ConstantManager();

	static DWORD WINAPI threadProc(LPVOID lpParam);
	void thread();
	
private:
	HANDLE hDir;

	void parseFile();

	std::vector<int> intVec;
	std::vector<float> floatVec;
	std::vector<std::string> stringVec;
	HANDLE threadHandle;
};