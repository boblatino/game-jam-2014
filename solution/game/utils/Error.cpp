#include "Error.h"

#include <iostream>
#include <SDL_error.h>
#include <cassert>

void printError(const char* msg, const bool includeSdlError /*= true*/) {
    std::cerr << msg;

    if (includeSdlError) {
        std::cerr << " - " << SDL_GetError();
    } 
	assert(false);
}
