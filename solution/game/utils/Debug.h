#pragma once

struct Point2;
struct Point3;
struct Vector3;

// Prints p coordinates in standard output
void printPoint2(const Point2& p, const char* info);

// Prints p coordinates in standard output
void printPoint3(const Point3& p, const char* info);

// Prints vec coordinates in standard output
void printVector3(const Vector3& v, const char* info);
