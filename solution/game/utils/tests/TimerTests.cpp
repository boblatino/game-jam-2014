#include "TimerTests.h"

#include <iostream>
#include <windows.h>

#include "utils/Timer.h"

void printInfo(const float f, const char* info) {
    std::cout << info << " " << f << std::endl;
}

void init(Timer& timer) {
    timer.reset();
    timer.start();
}

void execute(Timer& timer) {    
    while (true) {
        Sleep(4000);
        timer.tick();
        printInfo(timer.activeTime(), "In Game Time");
        printInfo(timer.deltaTime(), "Delta Time");
    }
}

void clear(Timer& timer) {
    timer.stop();
}

namespace timerTests {
    void run() {
        Timer timer;
        init(timer);
        execute(timer);
        clear(timer);
    }
}