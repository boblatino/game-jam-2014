#include "MusicTests.h"


#include <SDL.h>
#include <SDL_mixer.h>

#include "main/Globals.h"
#include "main/MainConstants.h"
#include "utils/Error.h"
#include "utils/Timer.h"

namespace {
    Mix_Music* gMusic = nullptr;

    bool init() {
        Globals::init();
        Globals::gTimer.reset();

        gMusic = Mix_LoadMUS("test_media/sounds/music.mp3");
        if (!gMusic) {
            printError("Mix_LoadMUS failed");
            return false;
        }

        Mix_PlayMusic(gMusic, 0 /*no loops*/);

        return true;
    }

    void execute() {
        bool loop = true;
        while (loop) {
            Globals::gTimer.tick();

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    loop = false;
                    break;
                }
            }

            //Clear screen
            SDL_SetRenderDrawColor(Globals::gRenderer, 0, 0, 0, 0 );
            SDL_RenderClear(Globals::gRenderer);

            //Update screen
            SDL_RenderPresent(Globals::gRenderer);
        }
    }

    void clear() {
        Mix_HaltMusic();
        Mix_FreeMusic(gMusic);
        Globals::destroy();
    }
}

namespace musicTest {
    void run() {
        const bool result = init();
        if (!result) {
            printError("musicTests init() failed", false);
            return;
        }
        execute();
        clear();
    }
}