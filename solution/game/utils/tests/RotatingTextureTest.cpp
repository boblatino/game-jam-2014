#include "RotatingTextureTest.h"

#include <iostream>

#include <SDL.h>
#include <SDL_image.h>

#include "main/Globals.h"
#include "main/MainConstants.h"
#include "utils/Error.h"
#include "utils/Timer.h"

namespace {
    SDL_Texture* gTexture;

    bool init() {
        Globals::init();
        Globals::gTimer.reset();

        gTexture = IMG_LoadTexture(Globals::gRenderer, 
                                   "test_media/textures/circle_chain.png");
        if (!gTexture) {
            printError("IMG_LoadTexture failed");

            return false;
        }

        return true;
    }

    void execute() {
        bool loop = true;
        double rotationAngle = 0.0; 
        const double rotationFactor = -50.0f;
        while (loop) {
            Globals::gTimer.tick();

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    loop = false;
                    break;
                }
            }

            //Clear screen
            SDL_SetRenderDrawColor(Globals::gRenderer, 0, 0, 0, 0 );
            SDL_RenderClear(Globals::gRenderer);

            // Query texture width and height      
            int width;
            int height;
            SDL_QueryTexture(gTexture, nullptr, nullptr, &width, &height);
            SDL_Rect rect;
            rect.x = WINDOW_WIDTH / 4;
            rect.y = WINDOW_HEIGHT / 4;
            rect.w = width;
            rect.h = height;

            SDL_RenderCopyEx(Globals::gRenderer, 
                             gTexture, 
                             nullptr, 
                             &rect, 
                             rotationAngle * rotationFactor, 
                             nullptr, 
                             SDL_FLIP_NONE);

            //Update screen
            SDL_RenderPresent(Globals::gRenderer);

            // Update rotation angle using elapsed frame time
            rotationAngle += Globals::gTimer.deltaTime();
        }
    }

    void clear() {
        SDL_DestroyTexture(gTexture);
        Globals::destroy();
    }
}

namespace rotatingTextureTest {
    void run() {
        const bool result = init();
        if (!result) {
            printError("rotatingTextureTest init() failed", false);
            return;
        }

        execute();
        clear();
    }
}