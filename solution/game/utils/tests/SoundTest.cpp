#include "SoundTest.h"


#include <SDL.h>
#include <SDL_mixer.h>

#include "main/Globals.h"
#include "main/MainConstants.h"
#include "utils/Error.h"
#include "utils/Timer.h"

namespace {
    Mix_Chunk* gSound = nullptr;
    int gChannel = -1;

    bool init() {
        Globals::init();
        Globals::gTimer.reset();

        gSound = Mix_LoadWAV("test_media/sounds/sound.wav");
        if (!gSound) {
            printError("Mix_LoadMUS failed");
            return false;
        }

        gChannel = Mix_PlayChannel(-1, gSound, -1 /*infinite loop*/);

        return true;
    }

    void execute() {
        bool loop = true;
        while (loop) {
            Globals::gTimer.tick();

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    loop = false;
                    break;
                }
            }

            //Clear screen
            SDL_SetRenderDrawColor(Globals::gRenderer, 0, 0, 0, 0 );
            SDL_RenderClear(Globals::gRenderer);

            //Update screen
            SDL_RenderPresent(Globals::gRenderer);
        }
    }

    void clear() {
        Mix_HaltChannel(gChannel);
        Mix_FreeChunk(gSound);
        Globals::destroy();
    }
}

namespace soundTest {
    void run() {
        const bool result = init();
        if (!result) {
            printError("soundTest init() failed", false);
            return;
        }
        execute();
        clear();
    }
}