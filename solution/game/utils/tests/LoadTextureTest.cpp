#include "LoadTextureTest.h"

#include <iostream>

#include <SDL.h>
#include <SDL_image.h>

#include "main/Globals.h"
#include "main/MainConstants.h"
#include "utils/Error.h"
#include "utils/Timer.h"

namespace {
    SDL_Texture* gTexture;

    bool init() {
        Globals::init();
        Globals::gTimer.reset();

        gTexture = IMG_LoadTexture(Globals::gRenderer, "test_media/textures/circle.png");
        if (!gTexture) {
            printError("IMG_LoadTexture failed");
            return false;
        }

        return true;
    }

    void execute() {
        bool loop = true;
        while (loop) {
            Globals::gTimer.tick();

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    loop = false;
                    break;
                }
            }

            //Clear screen
            SDL_SetRenderDrawColor(Globals::gRenderer, 0, 0, 0, 0 );
            SDL_RenderClear(Globals::gRenderer);

            // Query texture width and height      
            int width;
            int height;
            SDL_QueryTexture(gTexture, nullptr, nullptr, &width, &height);
            SDL_Rect rect;
            rect.x = WINDOW_WIDTH / 4;
            rect.y = WINDOW_HEIGHT / 4;
            rect.w = width;
            rect.h = height;

            SDL_RenderCopyEx(Globals::gRenderer, 
                             gTexture, 
                             nullptr, 
                             &rect, 
                             0.0, 
                             nullptr, 
                             SDL_FLIP_NONE);

            //Update screen
            SDL_RenderPresent(Globals::gRenderer);

            std::cout << "Elapsed time = " << Globals::gTimer.deltaTime() << std::endl;
        }
    }

    void clear() {
        SDL_DestroyTexture(gTexture);
        Globals::destroy();
    }
}

namespace loadTextureTest {
    void run() {
        const bool result = init();
        if (!result) {
            printError("loadTextureTest init() failed", false);
            return;
        }
        execute();
        clear();
    }
}