#pragma once

#include "Color.h"

#define RED_COLOR   ( Color(255, 0, 0) )
#define BLUE_COLOR  ( Color(0, 255, 0) )
#define GREEN_COLOR ( Color(0, 0, 255) )
#define BLACK_COLOR ( Color(0, 0, 0) )
#define WHITE_COLOR ( Color(255, 255, 255) )

// Sound volume based on SDL Mixer values
#define MIN_VOLUME ( 0 )
#define MAX_VOLUME ( 128 )
#define MID_VOLUME ( MAX_VOLUME / 2 )