#pragma once

#include <cstdint>

struct Color {
    uint8_t r;
    uint8_t g;
    uint8_t b;
    
    Color(const uint8_t rColor = 0,
          const uint8_t gColor = 0,
          const uint8_t bColor = 0);
};