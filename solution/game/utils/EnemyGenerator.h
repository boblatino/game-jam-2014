#pragma once

#include <graphic\Drawable.h>
#include <vector>
#include <math/Point2.h>

class EnemyGenerator {
public:
	EnemyGenerator(size_t enemyCount, size_t speed);
	~EnemyGenerator();

	void generateStaticObjects(size_t rigidCount);

	void stopMotion();
	void startMotion();
	void generateChilds(size_t count, size_t speed);
	void generateLevelThings(size_t count);
	void rotateThings(float angle);


private:
	std::vector<std::string> staticObjId;
	std::vector<std::string> enemyObjId;
	std::vector<std::string> childObjId;

	std::vector<std::string> thingsObjId;

	std::vector<Drawable*> enemies;
	std::vector<Drawable*> staticObjs;
	std::vector<Drawable*> childs;
	std::vector<Drawable*> things;
	std::vector<Point2> originalPosOfThings;
};