#include "ResourceManager.h"

#include <SDL.h>
#include <SDL_image.h>
#include <utils/Error.h>
#include <main/Globals.h>

ResourceManager::ResourceManager() {
    loadTexture("background.png", "background");
    loadTexture("background0.png", "background0");
    loadTexture("background1.png", "background1");
    loadTexture("background2.png", "background2");
    loadTexture("background3.png", "background3");
    loadTexture("background4.png", "background4");
    loadTexture("background5.png", "background5");
    loadTexture("background6.png", "background6");
    loadTexture("background7.png", "background7");
    loadTexture("mantis_move.png", "mantis");
    loadTexture("shark_move2.png", "shark");	
	loadTexture("bomb.png", "mine");	
	loadTexture("puerta2bcerrada.png", "doorClosed");

	// childs
	loadTexture("child0.png", "child0");
	loadTexture("child1.png", "child1");
	loadTexture("child2.png", "child2");

	// Door
	loadTexture("doorOpening.png", "doorOpening");
	loadTexture("doorClosing.png", "doorClosing");
	loadTexture("doorOpened.png", "doorOpened");

    // Sounds
    loadSound("addChild.wav", "addChild");
    loadSound("removeChild.wav", "removeChild");
    loadSound("gameover.wav", "gameover");

    loadMusic("music.mp3", "music");

	// Exploding child
	loadTexture("Deathchild00.png", "child0Die");
	loadTexture("Deathchild01.png", "child1Die");
	loadTexture("Deathchild02.png", "child2Die");
	loadTexture("Deathchild03.png", "child3Die");

	// props
	loadTexture("p1.png", "p1");
	loadTexture("p2.png", "p2");
	loadTexture("p3.png", "p3");
	loadTexture("p4.png", "p4");
	loadTexture("p5.png", "p5");
	loadTexture("p6.png", "p6");
	loadTexture("p7.png", "p7");

	// Manits
	loadTexture("mantisDie.png ", "mantisDie");

	loadTexture("sharkEats.png ", "sharkEats");
	loadTexture("mantisTurn.png ", "mantisTurn");
	
	

}

ResourceManager::~ResourceManager() {
	for(std::map<std::string, SDL_Texture*>::iterator it = textures.begin(); it != textures.end(); ++it) {
		SDL_DestroyTexture(it->second);
	}

    for(std::map<std::string, Mix_Chunk*>::iterator it = sounds.begin(); 
                                                    it != sounds.end(); 
                                                    ++it) {
        Mix_FreeChunk(it->second);
    }

    for(std::map<std::string, Mix_Music*>::iterator it = music.begin(); 
                                                    it != music.end();
                                                    ++it) {
            Mix_FreeMusic(it->second);
    }
}

void ResourceManager::loadTexture(const std::string& fileName, const std::string& id, bool isTest) {
	const std::string path = isTest ? "test_media/textures/" : "media/textures/";
	SDL_Texture* texture = IMG_LoadTexture(Globals::gRenderer, (path + fileName).c_str());
	if (!texture) {
		printError("IMG_LoadTexture failed");
	}
	textures[id] = texture;
}

void ResourceManager::loadSound(const std::string& filename, const std::string& id) {
    const std::string path = "media/sounds/";
    Mix_Chunk* chunk = Mix_LoadWAV((path + filename).c_str());

    if (!chunk) {
        printError("Mix_LoadWAV failed");
        assert(false);
    }

    sounds[id] = chunk;
}

SDL_Texture* ResourceManager::getTexture(const std::string& id) {
	assert(textures.find(id) != textures.end());
	return textures[id];
}

Mix_Chunk* ResourceManager::getSound(const std::string& id) {
    assert(sounds.find(id) != sounds.end());
    return sounds[id];
}

Mix_Music* ResourceManager::getMusic(const std::string& id) {
    assert(music.find(id) != music.end());
    return music[id];
}

void ResourceManager::loadMusic(const std::string& filename, const std::string& id) {
    const std::string path = "media/sounds/";
    Mix_Music* file = Mix_LoadMUS((path + filename).c_str());

    if (!file) {
        printError("Mix_LoadMUS failed");
        assert(false);
    }

    music[id] = file;
}
	
