#pragma once

#include <cstdint>
#include <windows.h>

//////////////////////////////////////////////////////////////////////////
//
// Timer:
//    - Can be started, stopped and reset
//    - Can get the num of milliseconds it was active (not stopped)
//    - Can get the delta time (i.e. frame time)
//
//////////////////////////////////////////////////////////////////////////


struct Timer {
    uint64_t mLastStartTime;
    uint64_t mInPauseTime;
    uint64_t mLastStopTime;
    uint64_t mPreviousTickTime;
    uint64_t mCurrentTickTime;
    
    double mSecondsPerCount;
    double mDeltaTime; 
    
    bool mIsStopped;
    
    Timer();

    // Returns the total time elapsed since reset() was called, 
    // NOT counting any time when the clock is stopped.
    float activeTime() const;

    // Call before main loop. 
    // This should be called once in the application
    void reset(); 

    // Call when paused.
    void start(); 

    // Call when unpaused.
    void stop();  

    // Update delta time between last and current ticks
    // Update last tick time with time belonging to this tick
    void tick(); 

    // Returns current frame time
    double deltaTime() const;
    
private:
    Timer(const Timer& src);
    const Timer& operator=(const Timer& src);
};

