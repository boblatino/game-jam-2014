#pragma once

//////////////////////////////////////////////////////////////////////////
//
// Class that manages main character and its children
//
//////////////////////////////////////////////////////////////////////////

#include <vector>

#include <ai/steering/SteeringArrive.h>
#include <ai/steering/SteeringOutput.h>
#include <main/Iupdateable.h>
#include <physic/Kinematic.h>

class Animation;
class AnimatedDrawable;
class PursueData;

class CharacterManager : public Iupdateable {
public:
    void init();
    void clear();

    void addChild(AnimatedDrawable* drawable);

    // Returns true if a child could be removed.
    // It is useful to know if there were
    // remaining children because
    // otherwise, character should die
    bool removeChild();

    // Update character position according mouse position
    // Update steering behaviors
    void update();
    
    std::vector<Kinematic> mKinematics;
    std::vector<ArriveData> mInSteerings;
    std::vector<SteeringOutput> mOutSteerings;    
    std::vector<AnimatedDrawable*> mDrawables;

    Kinematic* mEnemyKinematic;
    PursueData* mEnemyInSteering;
    SteeringOutput* mEnemyOutSteering;
    AnimatedDrawable* mEnemyDrawable;

    // Used by player to pursue
    Point3 mMousePos;

    uint32_t mMinFramesToRemoveChild;
};