#pragma once

// Function that print msg passed by the user 
// and optionally SDL error info.
void printError(const char* msg, const bool includeSdlError = true);

