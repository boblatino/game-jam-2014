#pragma once
#include <graphic\Drawer.h>
//////////////////////////////////////////////////////////////////////////
//
// Generates a level based on 5 textures that represent
// lateral escapes and central game area
// Always we use textu
class Drawable;

struct ILevelGeneratorEvents {
	virtual void rotationStart() = 0;
	virtual void rotationEnd() = 0;
	virtual void cleanEnemies() = 0;
	virtual void finished() = 0;
};

class LevelGenerator {
public:
    LevelGenerator(ILevelGeneratorEvents* events);

    void update();

	void changeLevel();

    // 0: Center
    // 1: Left
    // 2: Right
    // 3: Top
    // 4: Bottom
	// 5: Top left
	// 6: Top right
	// 7: Bottom left
	// 8: Bottom right
	// 9: Door current level
	// 10: Door next level

	// 11: Next dw 1
	// 12: Next dw 2
	// 13: Next dw 3

    // Remember to avoid deleting those pointers
    // DrawManager is responsible
    Drawable* mDrawables[14];
    
    // Frames that passed since last rotation
    uint32_t mFramesSinceRot;
    
    uint32_t mFramesWhileRot;

    float mCurRotation;

    bool mRotate;
	ILevelGeneratorEvents* events;

	StaticData doorStartPos;

	bool panLeft;
	size_t panFrameCount;
	bool startPan;

	bool doorOpened;
};