#pragma once
#include <string>

struct VLCPlayer {
	VLCPlayer(const std::string& file);
	~VLCPlayer();
	void play();
	
	void pause(bool pause) {
		pauseState = pause; 
	}

	bool isVideoStopped;
private:
	std::string file;
	bool pauseState;
};