#pragma once

#include <cstdint>
#include <string>

#include <SDL_rect.h>

struct SDL_Texture;

struct TextureData;

// Structure used to store an atlas with several
// frames of animation.
// You can go to next animation and/or extract current one.
// We assume that animations are in row major order
struct Animation {
    // Atlas where frames are stored
    TextureData* mTexData;

    // Atlas width and height
    uint32_t mAtlasW;
    uint32_t mAtlasH;

	size_t frameCount;
    size_t mCurFrameCount;

    uint32_t mFrameW;
    uint32_t mFrameH;

    // Current frame of the texture
    SDL_Rect mCurFrame;

    bool mForwardPlay;
    bool mPlay;
    bool mPlayOnce;
    bool mDestroyable;

    Animation(TextureData& texData, const uint32_t frameW, const uint32_t frameH);

	// Reset animation to the first frame
	void reset();

	// Fill SDL_Rect with information about
	// current frame rectangle
	void currentFrameRect(SDL_Rect& rect);

    bool isPlaying() const { return mPlay; }
    bool destroyable() const { return mDestroyable; }

	// Update frame to the next one
	void update();

    void playOnce(const bool destroyable);
    void playOnceBackwards(const bool destroyable);
};