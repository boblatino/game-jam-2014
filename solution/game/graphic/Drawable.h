#pragma once
#include <string>
#include "Drawer.h"

#include <SDL.h>
#include <SDL_image.h>
#include "utils/Error.h"
#include "main/Globals.h"
#include <Dynamics/b2Body.h>

#include <Collision/Shapes/b2PolygonShape.h>
#include <Common/b2Math.h>

#include <collisions\Entity.h>

class Drawable : public Iupdateable {
public: 
	// Pass entity with new. It will be auto freed later
	Drawable(const std::string& id, 
             Entity* entity = nullptr, 
             const int* w = nullptr,
             const int* h = nullptr); 

	virtual ~Drawable(); 

	virtual void draw();

	void updatePosition(float x, float y, float orientation = 0.0f);
    void updateOrientation(const float orientation);
	void updateTexture(SDL_Texture* texture);
	const StaticData& getPosition();
	void setVelocity(b2Vec2 vel);
	void setConstantVelocity(b2Vec2 vel);
	virtual void update();
    void getTextureSize(int& w, int& h);
    virtual TextureData& texData() { return texture; }

	bool drawOnTop;
	std::string id;

protected:
	b2Body* body;
	StaticData worldSpace;
	TextureData texture;
	b2PolygonShape collider; 
	
    bool mAllowPhysics;

	bool enableConstantVelocity;


private: 
	Drawable(const Drawable&) {}
	const Drawable& operator=(const Drawable&) {}
};