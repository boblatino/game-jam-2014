#pragma once
#include <graphic\Drawable.h>
#include <graphic\Animation.h>
#include <main\IUpdateable.h>



struct AnimatedDrawable : public Drawable {

~AnimatedDrawable();

AnimatedDrawable(const std::string& atlas, Entity* entity, int w, int h); 

void setAnimationSpeed(float time) {
	animationSpeed = time;
}

void update();
void moveAnimation();

Animation& getAnimation() { return *otherAnimations[0]; }
void draw();
virtual TextureData& texData() ;

void appendAnimation(const std::string& atlas, int w, int h, size_t timeToToggle);


private:
	float animationSpeed;
	float currentTime;

	float currentFlipTime;
	float flipSpeed;
	size_t currentAnimIndex;

	Animation* currentAnim;

	std::vector<Animation*> otherAnimations;
};