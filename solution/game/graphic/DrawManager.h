#pragma once

#include <vector>
#include <graphic\Drawable.h>

// Takes ownership of the drawables
class DrawManager {
public:
	~DrawManager();
	void drawAll();
	void addDrawable(Drawable* drawable);
    void removeDrawable(Drawable* drawable);
    void clear();

	bool isRegistered(Drawable* drawable);

private:

	std::vector<Drawable*> drawables;
};