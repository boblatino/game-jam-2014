#include "GraphicUnitTests.h"

#include <cassert>

#include "graphic/Animation.h"
#include "graphic/Drawer.h"
#include "math/Utils.h"

namespace {
    void drawDataTests() {
        // DrawData::Constructor:
        {
            const float centerPosX = 10.0f;
            const float centerPosY = -15.0f;
            const float orientation = 1.0f;
            const DynamicData drawData(&centerPosX, &centerPosY, &orientation);

            assert(areEquals(*drawData.mCenterPosX, centerPosX) == true);
            assert(areEquals(*drawData.mCenterPosY, centerPosY) == true);
            assert(areEquals(*drawData.mOrientation, orientation) == true);
        }

        // set():
        {
            const float centerPosX = 10.0f;
            const float centerPosY = -15.0f;
            const float orientation = 1.0f;
            DynamicData drawData;
            drawData.set(centerPosX, centerPosY, orientation);

            assert(areEquals(*drawData.mCenterPosX, centerPosX) == true);
            assert(areEquals(*drawData.mCenterPosY, centerPosY) == true);
            assert(areEquals(*drawData.mOrientation, orientation) == true);
        }
    }
}

namespace graphicUnitTests {
    void runTests() {
        drawDataTests();
    }
}