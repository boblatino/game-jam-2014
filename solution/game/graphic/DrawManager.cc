#include "DrawManager.h"

#include <cassert>
#include <algorithm>

void DrawManager::drawAll() {
	for(size_t i = 0; i < drawables.size(); ++i) {
		if(drawables[i]->drawOnTop == false) {
			drawables[i]->draw();
		}
	}

	for(size_t i = 0; i < drawables.size(); ++i) {
		if(drawables[i]->drawOnTop) {
			drawables[i]->draw();
		}
	}
}

void DrawManager::addDrawable(Drawable* drawable) {
	drawables.push_back(drawable);
}

void DrawManager::removeDrawable(Drawable* drawable) {
    std::vector<Drawable*>::iterator it;
    it = std::find(drawables.begin(), drawables.end(), drawable);
    if(it != drawables.end()) {
		delete *it;
		drawables.erase(it);
	}
}

void DrawManager::clear() {
    for(size_t i = 0; i < drawables.size(); ++i) {
        delete drawables[i];
    }

    drawables.clear();
}

DrawManager::~DrawManager() {
	
}

bool DrawManager::isRegistered(Drawable* drawable) {
	return std::find(drawables.begin(), drawables.end(), drawable) != drawables.end();
}