#pragma once

#include <cstdint>

struct SDL_Rect;
struct SDL_Texture;

struct DynamicData {
    const float* mCenterPosX;
    const float* mCenterPosY;

    // Angle in radians around y axis from y to x axis
    const float* mOrientation;

    DynamicData(const float* centerPosX = nullptr,
        const float* centerPosY = nullptr,
        const float* orientation = nullptr);

    void set(const float& centerPosX,
             const float& centerPosY,
             const float& orientation);
};

struct StaticData {
    float mCenterPosX;
    float mCenterPosY;

    // Angle in radians around y axis from y to x axis
    float mOrientation;

    StaticData(const float centerPosX = 0.0f,
        const float centerPosY = 0.0f,
        const float orientation = 0.0f);

    void set(const float centerPosX,
             const float centerPosY,
             const float orientation);
};

// Structure that stores data about a texture:
// - Texture itself
// - Portion of the texture to be drawn
struct TextureData {
    SDL_Texture* mTexture;
    SDL_Rect* mRect;
    
    TextureData(SDL_Texture* texture = nullptr, SDL_Rect* rect = nullptr);
    ~TextureData();

    // Update texture and sets rectangle to be drawn
    // as the entire texture
    void updateTexture(SDL_Texture* tex);

    void updateRect(const SDL_Rect& rect);

};

// Note: numElems should be greater than zero
void drawTextures(const DynamicData * const data,
                  TextureData* const textures,
                  const uint32_t numElems);

// Note: numElems should be greater than zero
void drawTextures(const StaticData * const data,
                  TextureData* const textures,
                  const uint32_t numElems);

// Note: numElems should be greater than zero
void drawTextures(TextureData* const textures, const uint32_t numElems);