#pragma once

#include <cstdint>

#include "graphic/Drawer.h"

//////////////////////////////////////////////////////////////////////////
//
// Struct that represents a background with horizontal scrolling capability
//
// You should initialize it with an array of textures, each one of the
// same size.
//
// Texture width = N * window width
// Texture height = window height
//
// To get good results in ENDLESS mode:
// The first WINDOW_WIDTH pixels of a texture should be the same
// that the last WINDOW_WIDTH pixels of its previous texture.
// The last WINDOW_WIDTH pixels of a texture should be the same
// that the first WINDOW_WIDTH pixels of its next texture.
//
// It can be automatically scrolled at certain speed per frame
// or manually scrolled
//
// It can be scrolled in left or right directions
//
// It has an endless mode, i.e., a circular scrolling background
//
//////////////////////////////////////////////////////////////////////////

struct SDL_Texture;

enum class HScrollDirection {
    LEFT,
    RIGHT
};

struct HScrollableBackground {
    // Used to draw
    TextureData mTexData;

    // Textures that compose the entire background
    SDL_Texture* * const mTextures;
    const size_t mNumTex;

    // Scrolling direction
    HScrollDirection mDir;

    // Cache background width, i.e., sum
    // of all textures width
    uint32_t mBackgroundW;

    // Cache texture width
    uint32_t mTexWidth;

    // Current x coordinate where we need to 
    // position the entire texture (concatenation of all texture)
    // to be drawn
    int mCurX;

    // Scrolling speed in X axis a per frame basis
    int mSpeed;

    // Circular (endless) scrolling
    bool mEndless;

    HScrollableBackground(SDL_Texture* * const tex,
                          const size_t numTex,
                          const uint32_t speed, 
                          const HScrollDirection dir,
                          const bool endless);
};

// Reset background position to the initial one
// according scrolling direction
void reset(HScrollableBackground& background);

void changeSpeed(const uint32_t speed, HScrollableBackground& background);

// Change direction. If new direction is NONE, 
// speed will be set to zero
void changeDirection(const HScrollDirection dir, HScrollableBackground& background);

void setEndless(const bool b, HScrollableBackground& background);

// Update current background position based on
// direction and speed.
// This should be called once per frame.
void update(HScrollableBackground& background);

// Update current background position based on
// a new speed.
// This ignores background speed and direction
// previously set.
// This should be called once per frame.
void update(const int speed, HScrollableBackground& background);