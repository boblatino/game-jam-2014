#include "AnimatedDrawable.h"
#include <graphic/DrawManager.h>
#include <main/Globals.h>
#include <main/mainconstants.h>

#include <utils\ResourceManager.h>

AnimatedDrawable::AnimatedDrawable(const std::string& atlas, Entity* entity, int w, int h) :
	Drawable(atlas, entity, &w, &h), currentTime(0.f), animationSpeed(0), currentFlipTime(0), flipSpeed(0), currentAnimIndex(0) {
		appendAnimation(atlas, w, h, 2);
		
		if(!mAllowPhysics) {
			Globals::gGameLoop->updateMe(this);
		}
		currentAnim = otherAnimations[0];
}

void AnimatedDrawable::update() {
	if(mAllowPhysics) {
		Drawable::update();
	}

    if (currentAnim->isPlaying() == false && currentAnim->destroyable()) {
		currentAnim->mDestroyable = false;
		Globals::gDrawManager->removeDrawable(this);
		return;
    }

	if(animationSpeed == 0) {
		return;
	}
	currentTime += MS_PER_FRAME_FLOAT;
	if(currentTime >= animationSpeed){
		currentTime = 0.f;
		currentAnim->update();
	}

	currentFlipTime+= MS_PER_FRAME_FLOAT;
	if(flipSpeed != 0 && currentFlipTime >= flipSpeed){
		currentFlipTime = 0.f;
		currentAnim = otherAnimations[currentAnimIndex];
		if(currentAnimIndex < otherAnimations.size() - 1) {
			currentAnimIndex++;
		} else {
			currentAnimIndex = 0;
		}
	}
}


void AnimatedDrawable::draw() {
	drawTextures(&worldSpace, currentAnim->mTexData, 1);
}


void AnimatedDrawable::moveAnimation() {
	currentAnim->update();
}

AnimatedDrawable::~AnimatedDrawable() {
	if(!mAllowPhysics) {
		Globals::gGameLoop->removeMe(this);
	}
}

TextureData& AnimatedDrawable::texData() {
	return *currentAnim->mTexData;
}

void AnimatedDrawable::appendAnimation(const std::string& atlas, int w, 
									   int h, size_t timeToToggle) 
{
	SDL_Texture* tex = Globals::gResourceManager->getTexture(atlas);	
	TextureData* data = new TextureData;
	data->mTexture = tex;

	Animation* otherAnim = new Animation(*data, w, h);
	flipSpeed = timeToToggle;
	otherAnimations.push_back(otherAnim);
}