#include <iostream>
#include <SDL.h>
#include <SDL_image.h>

#include "graphic/Animation.h"
#include "graphic/Drawer.h"
#include "main/Globals.h"
#include "main/MainConstants.h"
#include "math/Point2.h"
#include "utils/Error.h"
#include "utils/Timer.h"

#include "graphic/Drawable.h"
#include "graphic/DrawManager.h"

#include "DrawableTest.h"

DrawableTest::DrawableTest() {	
	/*d = new Drawable("box");
	d->updatePosition(20, 20);

	d2 = new Drawable("beeSmall");
	d2->updatePosition(150, 150);

	Globals::gDrawManager->addDrawable(d);
	Globals::gDrawManager->addDrawable(d2);
	Globals::gGameLoop->updateMe(this);
	count = 0;*/
}

DrawableTest::~DrawableTest() {
	Globals::gGameLoop->removeMe(this);
}

void DrawableTest::update() 
{
	if(count > 100) {
		count = 0;
		StaticData pos = d->getPosition();
		d->updatePosition(pos.mCenterPosX + 1, pos.mCenterPosY + 1);
	}
	count ++;
}

