#pragma once

//////////////////////////////////////////////////////////////////////////
//
// The application shows a vertical scrollable background.
// 'P' = pause/resume scrolling
// 'I' = invert scrolling direction (up/down)
// 'A / S' = increment / decrement scrolling speed
// 'E' = activate / deactivate endless mode
//
//////////////////////////////////////////////////////////////////////////

namespace verticalScrollingTest {
    void run();
}