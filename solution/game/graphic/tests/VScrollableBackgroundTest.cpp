#include "VScrollableBackgroundTest.h"

#include <cassert>
#include <iostream>
#include <string>

#define SDL_MAIN_HANDLED
#include <SDL.h>
#include <SDL_image.h>

#include "graphic/Drawer.h"
#include "graphic/VScrollableBackground.h"
#include "main/Globals.h"
#include "main/MainConstants.h"
#include "math/Point2.h"
#include "utils/Error.h"
#include "utils/Timer.h"

#define NUM_TEXTURES ( 4 )
#define SPEED_FACTOR ( 10 )
#define INITIAL_SPEED ( 20 )

namespace {
    SDL_Texture* gTextures[NUM_TEXTURES];
    std::string gTexturePaths[NUM_TEXTURES];
    VScrollableBackground* gBackground;
    int gSpeed = INITIAL_SPEED;

    // Info to change background behavior
    bool gPauseScrolling = false;
    VScrollDirection gScrollDirection = VScrollDirection::UP;
    bool gEndlessMode = true;

    bool init() {
        Globals::init();
        Globals::gTimer.reset();

        // Init textures
        gTexturePaths[0] = "test_media/textures/verticalBackgroundPart0.png";
        gTexturePaths[1] = "test_media/textures/verticalBackgroundPart1.png";
        gTexturePaths[2] = "test_media/textures/verticalBackgroundPart2.png";
        gTexturePaths[3] = "test_media/textures/verticalBackgroundPart3.png";

        for (size_t i = 0; i < NUM_TEXTURES; ++i) {
            gTextures[i] = IMG_LoadTexture(Globals::gRenderer, gTexturePaths[i].c_str());
            if (!gTextures[i]) {
                printError("IMG_LoadTexture failed");
                assert(false);
            }
        }

        // Init vertical scrollable background
        gBackground = new VScrollableBackground(gTextures, 
                                                NUM_TEXTURES, 
                                                INITIAL_SPEED, 
                                                gScrollDirection, 
                                                gEndlessMode);

        return true;
    }

    void execute() {
        bool loop = true;
        while (loop) {
            // Begin of frame tick
            Globals::gTimer.tick();

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    loop = false;
                    break;
                } else if (event.type == SDL_MOUSEMOTION) {
                    Globals::gMouseState.handleMouseMotionEvent(event.motion);
                } else if (event.type == SDL_KEYDOWN || event.type == SDL_KEYUP) {
                    Globals::gKeyboardState.handleKeyboardEvent(event.key);
                }
            }

            // 'P' = pause/resume scrolling
            if (Globals::gKeyboardState.keyPressedEvent(SDL_SCANCODE_P)) {
                std::cout << "Toggle Pause" << std::endl;
                gPauseScrolling = !gPauseScrolling;
            }

            // 'I' = invert scrolling direction (left/right)
            if (Globals::gKeyboardState.keyPressedEvent(SDL_SCANCODE_I)) {
                std::cout << "Invert direction" << std::endl;
                if (gScrollDirection == VScrollDirection::UP) {
                    gScrollDirection = VScrollDirection::DOWN;                    
                } else {
                    gScrollDirection = VScrollDirection::UP;
                }

                changeDirection(gScrollDirection, *gBackground);
            }

            // 'E' = activate / deactivate endless mode
            if (Globals::gKeyboardState.keyPressedEvent(SDL_SCANCODE_E)) {
                std::cout << "Toggle Endless mode" << std::endl;
                gEndlessMode = !gEndlessMode;
                setEndless(gEndlessMode, *gBackground);
            }

            // 'A / S' = increment / decrement scrolling speed
            if (Globals::gKeyboardState.keyPressedEvent(SDL_SCANCODE_A)) {
                std::cout << "Increment speed" << std::endl;
                gSpeed += SPEED_FACTOR;
                changeSpeed(gSpeed, *gBackground);
            }

            if (Globals::gKeyboardState.keyPressedEvent(SDL_SCANCODE_S)) {
                std::cout << "Decrement speed" << std::endl;
                gSpeed -= SPEED_FACTOR;
                if (gSpeed < 0) {
                    gSpeed = 0;
                }
                changeSpeed(gSpeed, *gBackground);
            }

            // Update background position
            if (!gPauseScrolling) {
                update(*gBackground);
            }            

            //Clear screen
            int result = SDL_SetRenderDrawColor(Globals::gRenderer, 0, 0, 0, 0 );
            if (result != 0) {
                printError("SDL_SetRenderDrawColor failed");
                loop = true;
                break;
            }

            result = SDL_RenderClear(Globals::gRenderer);
            if (result != 0) {
                printError("SDL_RenderClear failed");
                loop = true;
                break;
            }

            // Draw background
            drawTextures(&gBackground->mTexData, 1);

            //Update screen
            SDL_RenderPresent(Globals::gRenderer);

            Globals::gKeyboardState.clearKeyEvents();

            // End of frame tick
            // Ensure current frame takes at least
            // MS_PER_FRAME
            Globals::gTimer.tick();           
            double elapsedTime = Globals::gTimer.deltaTime();
            while (elapsedTime < MS_PER_FRAME_DOUBLE) {
                Globals::gTimer.tick();
                elapsedTime += Globals::gTimer.deltaTime();
            }   
        }
    }

    void clear() {
        delete gBackground;

        for (size_t i = 0; i < NUM_TEXTURES; ++i) {
            SDL_DestroyTexture(gTextures[i]);
        }

        Globals::destroy();
    }
}

namespace verticalScrollingTest {
    void run() {
        const bool result = init();
        if (!result) {
            printError("verticalScrollingTest init() failed", false);
            assert(false);
        }
        execute();
        clear();
    }
}