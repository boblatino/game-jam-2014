#include "AnimationTest.h"

#include <iostream>
#include <SDL.h>
#include <SDL_image.h>

#include "graphic/Animation.h"
#include "graphic/Drawer.h"
#include "main/Globals.h"
#include "main/MainConstants.h"
#include "math/Point2.h"
#include "utils/Error.h"
#include "utils/Timer.h"


#define FRAME_WIDTH  ( 160 )
#define FRAME_HEIGHT ( 198 )

namespace {
    TextureData gTexture;
    StaticData gDrawData;
    Animation* gAnimation;

    bool init() {
        Globals::init();
        Globals::gTimer.reset();

        SDL_Texture* texture;
        texture = IMG_LoadTexture(Globals::gRenderer, "test_media/textures/atlas.png");
        if (!texture) {
            printError("IMG_LoadTexture failed");
            return false;
        } else {
            // Init texture
            gTexture.mTexture = texture;

            // Init center positions
            gDrawData.mCenterPosX = WINDOW_WIDTH / 2;
            gDrawData.mCenterPosY = WINDOW_HEIGHT / 2;
        }

        // Init animation and init texture rect to be drawn
        gAnimation = new Animation("atlas", FRAME_WIDTH, FRAME_HEIGHT);
        SDL_Rect rect;
        gAnimation->currentFrameRect(rect);
        gTexture.updateRect(rect);

        return true;
    }

    void execute() {
        bool loop = true;
        while (loop) {
            Globals::gTimer.tick();

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    loop = false;
                    break;
                } else if (event.type == SDL_MOUSEMOTION) {
                    Globals::gMouseState.handleMouseMotionEvent(event.motion);
                } else if (event.type == SDL_KEYDOWN || event.type == SDL_KEYUP) {
                    Globals::gKeyboardState.handleKeyboardEvent(event.key);
                }
            }

            // Each time we press A, animation is updated
            if (Globals::gKeyboardState.keyPressedEvent(SDL_SCANCODE_A)) {
                std::cout << "Change Animation" << std::endl;
                gAnimation->update();
                gAnimation->currentFrameRect(*gTexture.mRect);
            }

            //Clear screen
            int result = SDL_SetRenderDrawColor(Globals::gRenderer, 0, 0, 0, 0 );
            if (result != 0) {
                printError("SDL_SetRenderDrawColor failed");
                loop = true;
                break;
            }

            result = SDL_RenderClear(Globals::gRenderer);
            if (result != 0) {
                printError("SDL_RenderClear failed");
                loop = true;
                break;
            }

            // Draw statics
            drawTextures(&gDrawData, &gTexture, 1);

            //Update screen
            SDL_RenderPresent(Globals::gRenderer);

            Globals::gKeyboardState.clearKeyEvents();
        }
    }

    void clear() {
        delete gAnimation;

        // We used the same texture for all the statics
        SDL_DestroyTexture(gTexture.mTexture);
        Globals::destroy();
    }
}

namespace animationTest {
    void run() {
        const bool result = init();
        if (!result) {
            printError("animationTest init() failed", false);
            return;
        }
        execute();
        clear();
    }
}