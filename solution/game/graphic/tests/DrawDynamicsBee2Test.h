#pragma once

//////////////////////////////////////////////////////////////////////////
//
// The application show a group of bees flying in the screen.
// - We applies hard-coded steering behavior to bees's kinematic.
// - Bees looks at linear velocity direction
//
//////////////////////////////////////////////////////////////////////////

namespace drawDynamicsBee2Test {
    void run();
}