#pragma once

//////////////////////////////////////////////////////////////////////////
//
// The application show a group of bees flying in the screen.
// - We applies hard-coded steering behavior to bees's kinematic.
// - Bees rotate and move in different directions.
//
//////////////////////////////////////////////////////////////////////////

namespace drawDynamicsBeeTest {
    void run();
}