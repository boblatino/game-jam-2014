#include "DrawStaticsTest.h"

#include <iostream>

#include <SDL.h>
#include <SDL_image.h>

#include "graphic/Drawer.h"
#include "main/Globals.h"
#include "main/MainConstants.h"
#include "math/Point2.h"
#include "utils/Error.h"
#include "utils/Timer.h"

#define NUM_STATICS ( 3 )

namespace {
    TextureData gTextures[NUM_STATICS];
    StaticData gDrawData[NUM_STATICS];

    bool init() {
        Globals::init();
        Globals::gTimer.reset();

        SDL_Texture* texture;
        texture = IMG_LoadTexture(Globals::gRenderer, "test_media/textures/circle_small.png");
        if (!texture) {
            printError("IMG_LoadTexture failed");
            return false;
        } else {
            // Init textures
            for (size_t i = 0; i < NUM_STATICS; ++i) {
                gTextures[i].mTexture = texture;
            }

            // Init center positions
            gDrawData[0].mCenterPosX = 0;
            gDrawData[0].mCenterPosY = 0;

            gDrawData[1].mCenterPosX = WINDOW_WIDTH / 2;
            gDrawData[1].mCenterPosY = WINDOW_HEIGHT / 2;

            gDrawData[2].mCenterPosX = WINDOW_WIDTH;
            gDrawData[2].mCenterPosY = WINDOW_HEIGHT;
        }

        return true;
    }

    void execute() {
        bool loop = true;
        while (loop) {
            Globals::gTimer.tick();

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    loop = false;
                    break;
                }
            }

            //Clear screen
            int result = SDL_SetRenderDrawColor(Globals::gRenderer, 0, 0, 0, 0 );
            if (result != 0) {
                printError("SDL_SetRenderDrawColor failed");
                loop = true;
                break;
            }
            
            result = SDL_RenderClear(Globals::gRenderer);
            if (result != 0) {
                printError("SDL_RenderClear failed");
                loop = true;
                break;
            }

            // Draw statics
            drawTextures(gDrawData, gTextures, NUM_STATICS);

            //Update screen
            SDL_RenderPresent(Globals::gRenderer);
            
            std::cout << "Elapsed time = " << Globals::gTimer.deltaTime() << std::endl;
        }
    }

    void clear() {
        // We used the same texture for all the statics
        SDL_DestroyTexture(gTextures[0].mTexture);
        Globals::destroy();
    }
}

namespace drawStaticsTest {
    void run() {
        const bool result = init();
        if (!result) {
            printError("drawStaticsTest init() failed", false);
            return;
        }
        execute();
        clear();
    }
}