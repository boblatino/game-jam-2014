#pragma once
#include <main/IUpdateable.h>

class Drawable;

struct DrawableTest : Iupdateable {

	DrawableTest();
	void update();
	~DrawableTest();

private:
	int count;
	Drawable* d;
	Drawable* d2;
};