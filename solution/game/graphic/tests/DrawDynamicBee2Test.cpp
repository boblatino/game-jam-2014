#include "DrawDynamicsBee2Test.h"

#include <iostream>

#include <SDL.h>
#include <SDL_image.h>

#include "ai/steering/SteeringOutput.h"
#include "graphic/Drawer.h"
#include "main/Globals.h"
#include "main/MainConstants.h"
#include "math/MathConstants.h"
#include "math/Point2.h"
#include "math/Vector3.h"
#include "math/Utils.h"
#include "physic/Kinematic.h"
#include "utils/Error.h"
#include "utils/Timer.h"

#define NUM_BEES ( 2 )

namespace {
    TextureData gTextures[NUM_BEES];
    DynamicData gDrawData[NUM_BEES];

    Kinematic gKinematics[NUM_BEES];
    SteeringOutput gSteerings[NUM_BEES];

    bool init() {
        Globals::init();
        Globals::gTimer.reset();

        SDL_Texture* texture;
        texture = IMG_LoadTexture(Globals::gRenderer, "test_media/textures/bee_small.png");
        if (!texture) {
            printError("IMG_LoadTexture failed");
            return false;
        } else {
            // Init textures
            for (size_t i = 0; i < NUM_BEES; ++i) {
                gTextures[i].mTexture = texture;
            }

            // Init kinematics
            gKinematics[0].mPosition.set(WINDOW_WIDTH / 3, 0.0f, 2 * (WINDOW_HEIGHT / 3));

            gKinematics[1].mPosition.set(2 * (WINDOW_WIDTH / 3), 0.0f, 2 * (WINDOW_HEIGHT / 3));

            gKinematics[0].mOrientation = 0.0f;
            gKinematics[1].mOrientation = 0.0f;

            gKinematics[0].mLinearVel.set(1.0f, 0.0f, -1.0f);
            gKinematics[1].mLinearVel.set(-1.0f, 0.0f, -1.0f);

            // Init steering outputs
            gSteerings[0].mLinearVel.set(10.0f, 0.0f, -10.0f);
            gSteerings[1].mLinearVel.set(-10.0f, 0.0f, 10.0f);
        }

        return true;
    }

    void execute() {
        bool loop = true;
        Globals::gTimer.reset();
        while (loop) {
            // Begin frame tick
            Globals::gTimer.tick();

            SDL_Event event;
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_QUIT) {
                    loop = false;
                    break;
                }
            }

            //Clear screen
            int result = SDL_SetRenderDrawColor(Globals::gRenderer, 0, 0, 0, 0 );
            if (result != 0) {
                printError("SDL_SetRenderDrawColor failed");
                loop = true;
                break;
            }

            result = SDL_RenderClear(Globals::gRenderer);
            if (result != 0) {
                printError("SDL_RenderClear failed");
                loop = true;
                break;
            }

            // Update kinematics
            const float dt = Globals::gTimer.deltaTime();
            integrate(gKinematics, gSteerings, NUM_BEES);

            const float orientation0 = 
                orientationFromVector(gKinematics[0].mLinearVel);

            const float orientation1 = 
                orientationFromVector(gKinematics[1].mLinearVel);

            // Update draw data
            gDrawData[0].mCenterPosX = &gKinematics[0].mPosition.x;
            gDrawData[0].mCenterPosY = &gKinematics[0].mPosition.z;
            gDrawData[0].mOrientation = &orientation0;

            gDrawData[1].mCenterPosX = &gKinematics[1].mPosition.x;
            gDrawData[1].mCenterPosY = &gKinematics[1].mPosition.z;
            gDrawData[1].mOrientation = &orientation1;

            // Draw 
            drawTextures(gDrawData, gTextures, NUM_BEES);

            //Update screen
            SDL_RenderPresent(Globals::gRenderer);

            std::cout << "Elapsed time = " << dt << std::endl;

            // End of frame tick
            // Ensure current frame takes at least
            // MS_PER_FRAME
            Globals::gTimer.tick();           
            double elapsedTime = Globals::gTimer.deltaTime();
            while (elapsedTime < MS_PER_FRAME_DOUBLE) {
                Globals::gTimer.tick();
                elapsedTime += Globals::gTimer.deltaTime();
            }  
        }
    }

    void clear() {
        // We used the same texture for all the statics
        SDL_DestroyTexture(gTextures[0].mTexture);
        Globals::destroy();
    }
}

namespace drawDynamicsBee2Test {
    void run() {
        const bool result = init();
        if (!result) {
            printError("drawDynamicsBee2Test init() failed", false);
            return;
        }
        execute();
        clear();
    }
}