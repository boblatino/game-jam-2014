#pragma once

//////////////////////////////////////////////////////////////////////////
//
// The application shows an horizontal scrollable background.
// 'P' = pause/resume scrolling
// 'I' = invert scrolling direction (left/right)
// 'A / S' = increment / decrement scrolling speed
// 'E' = activate / deactivate endless mode
//
//////////////////////////////////////////////////////////////////////////

namespace horizontalScrollingTest {
    void run();
}