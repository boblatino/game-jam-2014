#pragma once

//////////////////////////////////////////////////////////////////////////
//
// The applications draws statics. Here we want to test drawing mechanism
// and reposition of textures using center positions.
// We will draw in total 3 textures with center positions in:
// - Top Left Screen
// - Middle Screen
// - Bottom Right Screen
//
//////////////////////////////////////////////////////////////////////////

namespace drawStaticsTest {
    void run();
}