#pragma once

#include "VScrollableBackground.h"

#include <cassert>
#include <cstdlib>

#include <SDL_rect.h>
#include <SDL_render.h>

#include "main/MainConstants.h"
#include "utils/Error.h"

namespace {
    // Returns true if there are not
    // null textures.
    // Used in assert's to avoid
    // performance hit in Release
    bool checkNullTextures(SDL_Texture* * const tex, const size_t numTex) {
        for (size_t i = 0; i < numTex; ++i) {
            if (!tex[i]) {
                return false;
            }
        }

        return true;
    }

    // Returns true if all textures
    // have the same dimension
    bool checkTexturesDimensions(SDL_Texture* * const tex, const size_t numTex) {
        // Extract first texture dimensions
        int w;
        int h;
        const int result = SDL_QueryTexture(tex[0], 0, 0, &w, &h);
        if (result != 0) {
            printError("SDL_QueryTexture failed");
            assert(false);
        }

        for (size_t i = 1; i < numTex; ++i) {
            int tmpW;
            int tmpH;
            const int result = SDL_QueryTexture(tex[0], 0, 0, &tmpW, &tmpH);
            if (result != 0) {
                printError("SDL_QueryTexture failed");
                assert(false);
            }

            if (tmpW != w || tmpH != h) {
                return false;
            }
        }

        return true;
    }
}

VScrollableBackground::VScrollableBackground(SDL_Texture* * const tex,
                                             const size_t numTex,
                                             const uint32_t speed, 
                                             const VScrollDirection dir,
                                             const bool endless) 
                                             : mTextures(tex)
                                             , mNumTex(numTex)
                                             , mDir(dir)
                                             , mEndless(endless)
{
    // At least 1 texture
    assert(mNumTex > 0);

    // No textures should be nullptr
    assert(checkNullTextures(mTextures, mNumTex));

    // Textures should have the same size
    assert(checkTexturesDimensions(mTextures, mNumTex));

    // Compute first texture dimensions
    int w;
    int h;
    const int result = SDL_QueryTexture(mTextures[0], 0, 0, &w, &h);
    if (result != 0) {
        printError("SDL_QueryTexture failed");
        assert(false);
    }

    // Texture height == N * window height
    assert(h % WINDOW_HEIGHT== 0);

    // Texture width == window width
    assert(w >= WINDOW_WIDTH);

    // Cache background height, i.e., sum
    // of all textures height
    mBackgroundH = static_cast<uint32_t> (h) * mNumTex;

    // Cache texture height
    mTexHeight = static_cast<uint32_t> (h);

    // Initialize portion of the background and
    // texture to be shown based on scrolling direction
    SDL_Rect rect;
    rect.x = 0;
    rect.w = WINDOW_WIDTH;
    rect.h = WINDOW_HEIGHT;

    switch (dir) {
    case VScrollDirection::UP:
        rect.y = 0;
        mCurY = 0;
        mSpeed = speed;
        mTexData.mTexture = mTextures[0];
        break;

    case VScrollDirection::DOWN:
        rect.y = mBackgroundH - WINDOW_HEIGHT;
        mCurY = mBackgroundH - WINDOW_HEIGHT;
        mSpeed = -speed;
        mTexData.mTexture = mTextures[mNumTex - 1];
        break; 

    default:
        break;
    }

    mTexData.updateRect(rect);
}    

void reset(VScrollableBackground& background) {
    assert(background.mTexData.mRect);

    SDL_Rect rect;
    rect.x = 0;
    rect.w = WINDOW_WIDTH;
    rect.h = WINDOW_HEIGHT;

    switch (background.mDir) {
    case VScrollDirection::UP:
        rect.y = 0;
        background.mCurY = 0;
        break;

    case VScrollDirection::DOWN:
        rect.y = background.mBackgroundH - WINDOW_HEIGHT;
        background.mCurY = background.mBackgroundH - WINDOW_HEIGHT;
        break; 

    default:
        break;
    }

    background.mTexData.updateRect(rect);
}

void changeSpeed(const uint32_t speed, VScrollableBackground& background) {
    switch (background.mDir) {
    case VScrollDirection::UP:
        background.mSpeed = speed;
        break;
    case VScrollDirection::DOWN:
        background.mSpeed = -speed;
        break;
    default:
        break;
    }
}

void changeDirection(const VScrollDirection dir, VScrollableBackground& background) {
    background.mDir = dir;
    switch (dir) {
    case VScrollDirection::UP:
        background.mSpeed = abs(background.mSpeed);
        break;
    case VScrollDirection::DOWN:
        background.mSpeed = -abs(background.mSpeed);
        break;
    default:
        break;
    }
}

void setEndless(const bool b, VScrollableBackground& background) {
    background.mEndless = b;
}

void update(VScrollableBackground& background) {
    assert(background.mTexData.mRect);

    // Update current y coord
    background.mCurY += background.mSpeed;

    if (background.mEndless) {     
        if (background.mCurY < 0) {
            // Up scrolling case
            background.mCurY = background.mBackgroundH - WINDOW_HEIGHT;
        } else if (background.mCurY > background.mBackgroundH - WINDOW_HEIGHT) {
            // Down scrolling case
            background.mCurY = 0;
        }
    } else {                
        if (background.mCurY < 0) {
            // Up scrolling case
            background.mCurY = 0;
        } else if (background.mCurY > background.mBackgroundH - WINDOW_HEIGHT) {
            // Down scrolling case
            background.mCurY = background.mBackgroundH - WINDOW_HEIGHT;
        }
    }

    // Compute texture index
    const uint32_t texIndex = background.mCurY / background.mTexHeight;

    // Compute local texture y coord
    const uint32_t localY = background.mCurY % background.mTexHeight;

    // Update texture and rectangle to be shown
    background.mTexData.mTexture = background.mTextures[texIndex];
    background.mTexData.mRect->y = localY;
}

void update(const int speed, VScrollableBackground& background) {
    assert(background.mTexData.mRect);

    // Update current y coord
    background.mCurY += speed;

    if (background.mEndless) {     
        if (background.mCurY < 0) {
            // Up scrolling case
            background.mCurY = background.mBackgroundH - WINDOW_HEIGHT;
        } else if (background.mCurY > background.mBackgroundH - WINDOW_HEIGHT) {
            // Down scrolling case
            background.mCurY = 0;
        }
    } else {                
        if (background.mCurY < 0) {
            // Up scrolling case
            background.mCurY = 0;
        } else if (background.mCurY > background.mBackgroundH - WINDOW_HEIGHT) {
            // Down scrolling case
            background.mCurY = background.mBackgroundH - WINDOW_HEIGHT;
        }
    }

    // Compute texture index
    const uint32_t texIndex = background.mCurY / background.mTexHeight;

    // Compute local texture y coord
    const uint32_t localY = background.mCurY % background.mTexHeight;

    // Update texture and rectangle to be shown
    background.mTexData.mTexture = background.mTextures[texIndex];
    background.mTexData.mRect->y = localY;
}