#include "Point3.h"

#include <string.h>

#include "Vector3.h"
#include "Utils.h"


Point3::Point3(const float x1 /*= 0.0f*/, 
               const float y1 /*= 0.0f*/, 
               const float z1 /*= 0.0f*/)
               : x(x1)
               , y(y1)
               , z(z1)
{

}

bool Point3::operator==(const Point3& p) const {
    return areEquals(x, p.x) && 
           areEquals(y, p.y) &&
           areEquals(z, p.z);
}

bool Point3::operator!=(const Point3& p) const {
    return !(*this == p);
}

Point3& Point3::operator+=(const Vector3& v) {
    x += v.x;
    y += v.y;
    z += v.z;

    return *this;
}

Point3& Point3::operator-=(const Vector3& v) {
    x -= v.x;
    y -= v.y;
    z -= v.z;

    return *this;
}

void Point3::set(const float x1, const float y1, const float z1) {
    x = x1;
    y = y1;
    z = z1;
}

bool Point3::isZero() const {
    return areEquals(x, 0.0f) && 
           areEquals(y, 0.0f) &&
           areEquals(z, 0.0f);
}

Point3 Point3::operator+(const Vector3& v) const {
    const float newX = x + v.x;
    const float newY = y + v.y;
    const float newZ = z + v.z;

    return Point3(newX, newY, newZ);
}

Point3 Point3::operator-(const Vector3& v) const {
    const float newX = x - v.x;
    const float newY = y - v.y;
    const float newZ = z - v.z;

    return Point3(newX, newY, newZ);
}