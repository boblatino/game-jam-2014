#pragma once

struct Vector3;

// Returns true if num1 and num2 are equals.
// This function takes into account an epsilon
// for the comparison.
bool areEquals(const float num1, const float num2, const float epsilon = 1.0E-5);

// Computes unit vector using orientation angle around y-axis 
// and from z axis to x axis and stores it in v
void unitVectorFromOrientation(const float orientation, Vector3& v);

// Returns orientation based on the direction that vec points out.
float orientationFromVector(const Vector3& v);

// Get the angle in radians that we need to rotate
// from src to dest 
float rotationAngle(const Vector3& src, const Vector3& dest);

// Rotates vec around Y axis by angle
void rotateAroundYAxis(Vector3& v, const float angle);

// Returns a random float in the range [0.0f, max]
float randomFloat(const float max = 1.0f);

// Returns a random binomial in the range [-max, max]
float randomBinomial(const float max = 1.0f);

int randomInt(int min, int max);