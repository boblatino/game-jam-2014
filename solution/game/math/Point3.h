#pragma once

struct Vector3;

struct Point3 {
    float x;
    float y;
    float z;

    float mPadding; // To ensure 4word alignment

    Point3(const float x1 = 0.0f, 
          const float y1 = 0.0f, 
          const float z1 = 0.0f);

    void set(const float x1, const float y1, const float z1);

    bool isZero() const;

    bool operator==(const Point3& p) const;
    bool operator!=(const Point3& p) const;

    // Translate operation
    Point3& operator+=(const Vector3& v);
    Point3& operator-=(const Vector3& v);
    Point3 operator+(const Vector3& v) const;
    Point3 operator-(const Vector3& v) const;
};

