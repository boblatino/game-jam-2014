#pragma once

struct Point2 {
    float x;
    float y;

    Point2(const float x1 = 0.0f, const float y1 = 0.0f);

    void set(const float x1, const float y1);

    bool operator==(const Point2 p) const;
    bool operator!=(const Point2 p) const;
};

