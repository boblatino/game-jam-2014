#include "Point2.h"

#include "math/Utils.h"

Point2::Point2(const float x1 /*= 0.0f*/, 
               const float y1 /*= 0.0f*/)
               : x(x1)
               , y(y1)
{

} 

bool Point2::operator==(const Point2 p) const {
    return areEquals(x, p.x) && areEquals(y, p.y);
}

bool Point2::operator!=(const Point2 p) const {
    return !(*this == p);
}

void Point2::set(const float x1, const float y1) {
    x = x1;
    y = y1;
}