#pragma once

#define PI      ( static_cast<float> (3.14159265359) )
#define SQ_PI ( PI * PI )
#define HALF_PI ( static_cast<float> (1.57079632679) )

// n degrees * DEG2RAD = m radians
// n radians * RAD2DEG = m degrees
#define DEG2RAD ( PI / 180 )
#define RAD2DEG ( 180 / PI )