#include "UnitTest.h"

#include "ai/unitTests/AiUnitTests.h"
#include "graphic/unitTests/GraphicUnitTests.h"
#include "input/unitTests/InputUnitTests.h"
#include "math/unitTests/MathUnitTests.h"
#include "physic/unitTests/PhysicUnitTests.h"

void runAllTests() {
	aiUnitTests::runTests();
    graphicUnitTests::runTests();
    inputUnitTests::runTests();
    mathUnitTests::runTests();
    physicUnitTests::runTests();
}